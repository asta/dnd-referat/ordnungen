# Ordnungen der Studierendenschaft der Georg-August-Universität Göttingen

> Die Basis einer gesunden Ordnung ist ein großer Papierkorb.

— Kurt Tucholsky

## Website

Die Website mit den Ordnungen findest du aktuell unter https://asta.pages.gwdg.de/dnd-referat/ordnungen/

## Anleitung
Du findest die eigentlichen Ordnungen im Markdown-Format [hier](source/_posts).
