---
title: >-
    Ordnung der Studierendenschaft der Georg-August-Universität Göttingen über die Leistungen zur Milderung durch die Semestertickets verursachter finanzieller Härten (LeMSHO)
date: 2014-11-25
tags: "Semesterticket"
---

## I. ALLGEMEINER TEIL
### § 1 Zweck
Diese Ordnung regelt Leistungen der Studierendenschaft zur Milderung durch die gemäß §1 Abs. 4 BeitrO bestehenden Semestertickets verursachten sozialen Härten für ihre Mitglieder.

### § 2 Geltungsbereich
Diese Ordnung gilt
a) persönlich für die Mitglieder der Studierendenschaft der Georg-August-Universität Göttingen,
b) sachlich für finanzielle Unterstützung von Mitgliedern der Studierendenschaft durch die Studierendenschaft zur Erfüllung der Aufgaben gemäß § 1.

## II. SEMESTERTICKET-HÄRTEFALLREGELUNG
### § 3 Rechtsanspruch
Mitglieder der Studierendenschaft, für die die Entrichtung die Beiträge für die Semestertickets nach § 1 Abs. 4 Beitragsordnung der Studierendenschaft (BeitrO) eine unverhältnismäßige finanzielle Belastung darstellt, können nach Maßgabe dieser Ordnung eine Erstattung durch nach § 1 Abs. 4 BeitrO bereits geleisteten Beiträge für die Semestertickets erhalten. Ein Rechtsanspruch auf Erstattung nach den Regelungen dieser Ordnung besteht nicht.

### § 4 Antragsberechtigte
Antragsberechtigt sind die Mitglieder der Studierendenschaft, die die Beiträge für die Semestertickets gemäß § 1 Abs. 4 BeitrO für das Antragssemester entrichtet haben.

### § 5 Transparenz
Das AStA-Sozialreferat macht die Antragsfristen und die aktuellen Bemessungsgrenzen als Euro-Betrag bekannt. Dieses geschieht öffentlich über die Website und mittels Informationsmaterial.

### § 6 Antrag
(1) Der Antrag auf Erstattung der Semesterticketbeiträge, ist beim AStA-Sozialreferat oder AStA-Sekretariat einzureichen und muss enthalten:
a) das vollständig ausgefüllte und unterschriebene Formblatt gemäß Anlage 1 einschließlich der folgenden Nachweise:
aa) Immatrikulationsbescheinigung des Antragssemesters,
ab) unterschriebene formlose Einkommenserklärung,
ac) unterschriebene formlose Einkommenserklärung der Ehegattin oder des Ehegatten bzw. der eingetragenen Lebenspartnerin oder des eingetragenen Lebenspartners,
b) Nachweise über besondere notwendige Aufwendungen nach § 10 Abs. 2 und 3 falls zutreffend,
ba) eine gültige ärztliche Bescheinigung über eine vorliegende Schwangerschaft oder chronische Erkrankung,
bb) Behindertenausweis in Kopie,
bc) eine Krankenkassenbescheinigung gemäß §13a BAföG,
bd) Kopie der Geburtsurkunde des Kindes oder der Kinder und eine schriftliche Erklärung, dass das Kind oder die Kinder eine finanzielle Mehrbelastung darstellt bzw. darstellen.
be) eine schriftliche Erklärung, dass die Antragstellerin oder der Antragsteller allein für die Pflege und Erziehung des Kindes oder der Kinder sorgt.

(2) Antragstellende sind vom AStA darauf hinzuweisen, dass die zur Bearbeitung des Antrags erforderlichen Daten mindestens fünf Jahre gespeichert werden.

(3) Unvollständige Anträge können nicht bearbeitet werden. Können für den Antrag erforderliche Unterlagen nicht innerhalb der Antragsfrist erbracht werden und hat die oder der Antragsstellende dies nicht zu vertreten, so kann, wenn die Gründe für dieses Versäumnis gegenüber dem AStA-Sozialreferat nachgewiesen werden, eine Fristverlängerung gewährt werden.

### § 7 Antragsfrist
Der Antrag gemäß § 6 muss vollständig bis zum Ablauf des 30. Juni des laufenden Sommersemesters bzw. bis zum Ablauf des 15. Januar des laufenden Wintersemesters (Ausschlussfrist) beim AStA- Sozialreferat oder AStA-Sekretariat eingereicht werden.

### § 8 Antragsbearbeitung
(1) Über die Anträge entscheidet eine vom Studierendenparlament eingesetzte Kommission des Studierendenparlaments nach Maßgabe dieser Ordnung. Die Kommission nach Satz 1 hat sieben stimmberechtigte Mitglieder. Den Vorsitz führt ohne Stimmrecht die Sozialreferentin oder der Sozialreferent des AStA.

(2) Die Entscheidungen über die Anträge werden von der AStA-Sozialreferentin oder dem AStA-Sozialreferenten bekannt gegeben. Die Antragstellenden werden bei Antragsverfahren im Sommersemester bis zum Ablauf des 31. Juli und bei Antragsverfahren im Wintersemester bis zum Ablauf des 15. Februar per Mail durch das AStA-Sozialreferat über die Entscheidung über den Antrag informiert.

(3) Alle am Bearbeitungs- und Entscheidungsverfahren beteiligten Personen unterliegen zeitlich unbegrenzt der Verschwiegenheitspflicht bezüglich der personenbezogenen Daten der Antragstellenden. Vor der Beteiligung am Verfahren sind die Beteiligten durch eine schriftliche Verschwiegenheitserklärung unter Nennung der Rechtsfolgen zu verpflichten.

### § 9 Härtefallfonds
Das Studierendenparlament weist im Haushalt der Studierendenschaft semesterbezogene Mittel aus, welche für die Erstattung der Semesterticketbeiträge verwendet werden.

### § 10 Einkommensgrenze
(1) Als Einkommensgrenze wird der Bedarf entsprechend § 13 Abs. 1 und 2 BAföG in der jeweils gültigen Fassung angewandt ggf. zuzüglich eines Pauschalbetrags für Sondertatbestände nach Absatz 2, und eines Betrags entsprechend § 23 Abs. 1 Satz 1 Nr. 3 BAföG pro Kind bis zum vollendeten 18. Lebensjahr abzüglich des bezogenen Kindergeldes, soweit es sich hierbei nicht um das für das eigene Kind ausgezahlte Kindergeld handelt. Befindet sich die oder der Antragsstellende während des Antragssemesters im Ausland, so wird für die im Antragssemester verbrachte Zeit im Ausland der Bedarfssatz gemäß § 13 Abs. 4 BAföG i. V. m. BAföG-AuslandszuschlagsV als Einkommensgrenze geltend gemacht. Befindet sich die oder der Antragsstellende in einer Ehe oder eingetragenen Lebenspartnerschaft, so werden die Einkommen beider addiert und wird die Hälfte dieser Summe als Einkommen berücksichtigt. Abweichend hiervon kann die gemäß dieser Ordnung eingesetzte Kommission die Einkommen des Paares bis maximal der Hälfte des Einkommens unterschiedlich gewichten.

(2) Besondere notwendige Aufwendungen können als Sondertat-bestände geltend gemacht werden; hierfür wird ein Pauschalbetrag für Sondertatbestände von insgesamt 50 Euro pro Monat gewährt. Sondertatbestände nach Satz 1 sind insbesondere:
a) Schwangerschaft (auch der Ehegattin oder eingetragenen Lebenspartnerin, sofern in diesem Fall ein Mehrbedarf nachgewiesen wird),
b) chronische Erkrankung,
c) Behinderung im Sinne des § 2 Abs. 1 SGB IX, die nicht zur kostenfreien Nutzung des öffentlichen Personennahverkehrs berechtigt,
d) wenn die Antragstellerin oder der Antragsteller mit einem oder mehreren minderjährigen Kindern zusammenlebt und allein für deren Pflege und Erziehung sorgt.

(3) Weitere notwendige Aufwendungen können bei entsprechendem Nachweis die Kosten für eine Kranken- und Pflegeversicherung nach §13a Abs 1 und 2 BAföG sein.

(4) Antragstellende, deren monatliches Einkommen über der individuellen Einkommensgrenze nach Absatz 1 liegt, sind von einer Erstattung der Semesterticketbeiträge und vom Verfahren nach § 11 ausgeschlossen.

### § 11 Reihung der Antragstellenden
(1) Alle Antragstellenden, die die Erstattung der Semesterticketbeiträge beantragt haben, werden gemäß dem Absatz 2 zu errechnenden Fehlbetrag gereiht. Antragstellende mit jeweils gleichem monatlichem Fehlbetrag werden auf dem gleichen Listenplatz gereiht.

(2) Als monatlicher Fehlbetrag gilt die Differenz zwischen der Einkommensgrenze nach § 10 Abs. 1 bis 3 und dem monatlichen Einkommen der oder des jeweiligen Antragstellenden unter Abzug der berücksichtigungsfähigen Sondertatbestände nach § 10 Abs. 1, 2 und Abs.

### § 12 Erstattung der Semesterticketbeiträge
(1) Die Erstattung der Semesterticketbeiträge wird gemäß ihrer Reihung nach § 11 Abs. 1, beginnend mit dem höchsten individuellen monatlichen Fehlbetrag, so vielen Antragstellenden zugesprochen, dass der gemäß § 9 zur Verfügung stehende Betrag nicht überschritten wird. Die Zahl der Antragstellenden, welchen die Erstattung nach Satz 1 zugesprochen wird, reduziert sich entsprechend, falls aufgrund eines nach § 11 Abs. 1, Satz 2 von mehreren Antragstellenden besetzten Listenplatzes nicht alle von diesen Antragstellenden berücksichtigt werden können.

(2) Der AStA überweist den Erstattungsbetrag durch Banküberweisung.

## III. SCHLUSSBESTIMMUNGEN

### § 13 Inkrafttreten
Diese Ordnung tritt am Tage nach ihrer Veröffentlichung in den Amtlichen Mitteilungen I der Georg- August- Universität Göttingen in Kraft. Zugleich tritt die Ordnung der Studierendenschaft der Georg-August-Universität Göttingen über die Leistungen zur Milderung durch die Semestertickets verursachter finanzieller Härten (LeMSHO) in der Fassung der Bekanntmachung vom 06.05.2011 (Amtliche Mitteilungen der Georg-August-Universität Göttingen vom 06.05.2011/Nr. 10 S. 703); zuletzt geändert durch Beschluss des Studierendenparlaments der Georg-August-Universität Göttingen vom 18.12.2013 (Amtliche Mitteilungen I 2/2014 S. 10) außer Kraft.

