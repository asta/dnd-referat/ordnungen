---
title: Urabstimmungsordnung der Studierendenschaft der Georg August Universität Göttingen (UrabO)
date: 2009-11-20
tags:
  - Urabstimmungsordnung
  - UrabO
  - Studierendenschaft
fassung_der_bekanntmachung:
  date: 2004-05-19
  ort: Amtliche Mitteilungen Nr. 5/2004 S. 336
zuletzt_geaendert:
  gremium: Studierendenparlament
  date: 2009-11-03
  ort: Amtliche Mitteilungen Nr. 39/2009 vom 20.11.2009 S. 5901
---

Urabstimmungsordnung der Studierendenschaft in der Fassung der Bekanntmachung vom 19.05.2004 (Amtliche Mitteilungen Nr. 5/2004 S. 336), zuletzt geändert gemäß Beschluss des Studierendenparlaments vom 03. 11.2009 (Amtliche Mitteilungen Nr. 39/2009 vom 20.11.2009 S. 5901)

# Urabstimmungsordnung der Studierendenschaft der Georg August Universität Göttingen (UrabO)

## Abschnitt I: Die Urabstimmung

### §1 Grundsätze

1. Diese Ordnung regelt die Durchführung von Urabstimmungen der Studierendenschaft im Rahmen der Bestimmungen der Organisationssatzung der Studierendenschaft der Georg-August-Universität Göttingen (OrgS).
2. Im Übrigen gelten für die Urabstimmung die Bestimmungen der Wahlordnung der Studierendenschaft (StudWO) entsprechend, und, soweit diese keine Regelungen enthält, die Bestimmungen der Wahlordnung für die Wahlen zu den Kollegialorganen an der Georg-August-Universität Göttingen (UniWO) entsprechend.

### §2 Wahlausschuss

1. Der studentische Wahlausschuss nach §2 Abs. 2 StudWO, im Folgenden als Wahlausschuss bezeichnet, überwacht die Ordnungsmäßigkeit der Urabstimmung und ist für die Urabstimmung in Zusammenarbeit mit der Urabstimmungsleitung verantwortlich.
2. Der Wahlausschuss entscheidet Zweifelsfragen bei der Urabstimmungshandlung und Stimmenauszählung, stellt das Ergebnis der Urabstimmung fest und entscheidet über Einsprüche.
3. Der Wahlausschuss entscheidet über Widersprüche gegen Verwaltungsakte der Urabstimmungsorgane.

### §3 Urabstimmungsleitung

1. Die Urabstimmungsleitung obliegt der Präsidentin oder dem Präsidenten des Studierendenparlaments. Sie oder er ist für die ordnungsgemäße Vorbereitung und Durchführung der Urabstimmung verantwortlich.
2. Die Urabstimmungsleitung legt den Zeitplan für die Vorbereitung von Urabstimmungen mit den Auslegungs-, Einspruchs- und Einreichungsfristen im Benehmen mit dem Wahlausschuss fest, soweit dieser nicht zuständig ist.
3. Die Urabstimmungsleitung zieht nach §4 Abs. 6 OrgS zur Durchführung ihrer Aufgaben in der Regel den AStA heran.
4. Die Urabstimmungsleitung kann für die Koordinierung der Aufgaben in den Urabstimmungslokalen Beauftragte aus der Studierendenschaft bestellen.
5. Die Urabstimmungsleitung und die örtlichen Beauftragten können für die Beaufsichtigung der Urabstimmungshandlung sowie für die Auszählung Urabstimmungshelferinnen und Urabstimmungshelfer aus der Studierendenschaft bestellen.

### §4 Wählerverzeichnis

1. Abstimmen dürfen nur die Mitglieder der Studierendenschaft, die in das Wählerverzeichnis eingetragen sind.
2. Eine schriftliche Benachrichtigung der Abstimmungsberechtigten über die Eintragung in das Wählerverzeichnis braucht nicht zu erfolgen.

### §5 Wahlparallele und wahlunabhängige Urabstimmungen

1. Urabstimmungen sind nach dem Zeitpunkt ihrer Durchführung zu unterscheiden.
2. Urabstimmungen, die parallel zu Wahlen zum Studierendenparlament stattfinden, werden als wahlparallele Urabstimmungen bezeichnet. Sonstige Urabstimmungen werden als wahlunabhängige Urabstimmungen bezeichnet.

### §6 Bekanntmachung

1. Die Urabstimmungsleitung hat die Urabstimmung durch eine Urabstimmungsbekanntmachung hochschulöffentlich bekannt zu machen. Die Urabstimmungsbekanntmachung muss angeben:
    a) die Aufforderung zur Stimmabgabe mit dem Hinweis auf den Urabstimmungszeitraum, die Urabstimmungslokale und die Tageszeiten für die Stimmabgabe,
    b) alle zur Urabstimmung gestellten Anträge im Wortlaut,
    c) einen Hinweis darauf, dass jeder Antrag mit „ja“ oder „nein“ zu beantworten ist,
    d) die Regelungen für die Stimmabgabe und die Briefwahl mit Angabe der Frist für die Briefwahlanträge

    Bekanntmachungen wahlunabhängiger Urabstimmungen müssen außerdem angeben:
    e) die Aufforderung zur Einsichtnahme in das Wählerverzeichnis nach §21 Abs. 3 mit dem Hinweis auf die Möglichkeit, Einspruch einzulegen, auf die Einspruchsfrist sowie auf Ort und Zeit für die Abgabe von Einsprüchen,
    f) die Aufforderung zur Abgabe einer Zugehörigkeitserklärung nach §21 Abs. 4 innerhalb der Einspruchsfrist.

    Bekanntmachungen wahlparalleler Urabstimmungen müssen außerdem angeben:
    g) den Hinweis, dass das Wählerverzeichnis für die Urabstimmung dem Wählerverzeichnis für die Wahl zum Studierendenparlament entspricht,
    h) den Hinweis, dass die Urabstimmung parallel mit der Wahl zum Studierendenparlament und ggf. weiteren Wahlen im Wahllokal durchgeführt wird.
2. Die Bekanntmachung muss mindestens drei Wochen vor Beginn des Urabstimmungszeitraums beginnen und sich mindestens bis inklusive dem letzten Tag des Urabstimmungszeitraums erstrecken.

### §7 Zusammenfassung von Urabstimmungsanträgen

1. Stehen mehrere sich einander nicht berührende Anträge zur Urabstimmung an, so sollen diese im gleichen Zeitraum zur Urabstimmung (zusammengefasste Urabstimmungen) gestellt werden.
2. Sich einander berührende Anträge dürfen nicht in einer Urabstimmung zusammengefasst werden. In diesem Fall entscheidet der Wahlausschuss über das weitere Vorgehen. Dabei sind die Anträge grundsätzlich in der Reihenfolge des Eingangs nach §8 Abs. 2 bzw. der Anmeldung als Tagesordnungspunkt für eine Sitzung des Studierendenparlaments zur Abstimmung zu stellen.

### §8 Antrag auf Urabstimmung

1. Ein Antrag auf Urabstimmung nach §4 Abs. 1 Lit. a OrgS muss innerhalb von sechs Wochen von der erforderlichen Anzahl Mitglieder der Studierendenschaft unterzeichnet werden. Als Bezugsgröße gilt die Anzahl der Wahlberechtigten nach dem zuletzt erstellten Wählerverzeichnis, Stichtag ist der Beginn der Unterzeichnungsfrist.
2. Der Antrag ist bei der Präsidentin oder dem Präsidenten des Studierendenparlaments gemäß Abs. 7 schriftlich anzumelden. Dabei ist eine Verantwortliche oder ein Verantwortlicher aus der Studierendenschaft zu benennen.
3. Für die Sammlung der Unterschriften ist das von der Präsidentin oder dem Präsidenten des Studierendenparlaments zur Verfügung zu stellende Unterschriftenformular in unveränderter Form zu verwenden, Kopieren ist erlaubt. Dabei ist auf jeder Seite der zur Urabstimmung zu stellende Antrag abzudrucken. Weiterhin sind die Formulare derart zu gestalten, dass die Unterzeichnerinnen und Unterzeichner neben ihrer Unterschrift jeweils Name, Vorname, Geburtsdatum, Semesteranschrift und das Datum der Unterzeichnung einzutragen haben. Eine Unterzeichnung ohne die korrekte und vollständige Angabe dieser Daten ist ungültig; hierauf ist auf dem Formular hinzuweisen.
4. Die Unterzeichnungsfrist beginnt mit der Übergabe des Formulars durch die Präsidentin oder den Präsidenten des Studierendenparlaments an die Verantwortliche oder den Verantwortlichen nach Abs. 2 S. 2.
5. Die Unterschriftenlisten sind bei der Präsidentin oder dem Präsidenten des Studierendenparlaments innerhalb der Unterzeichnungsfrist einzureichen und von dieser oder diesem einer angemessen Überprüfung zu unterziehen. Sobald das erforderliche Quorum nach Abs. 1 erreicht ist, hat die Präsidentin oder der Präsident das Studierendenparlament zur Behandlung des Antrags einzuberufen.
6. Beschließt das Studierendenparlament den Antrag, so ist keine Urabstimmung über diesen Antrag durchzuführen.
7. Die Frage in Anträgen nach §4 Abs. 1 Lit. a in Verbindung mit §4 Abs. 3 OrgS ist daher so zu formulieren, dass die Antragstellerinnen und Antragsteller ihr Anliegen als erfüllt ansehen, wenn das Studierendenparlament durch entsprechenden Beschluss die Frage im Antrag positiv beantwortet.
8. Beschließt das Studierendenparlament nicht im Sinne von Abs. 7, so hat die Präsidentin oder der Präsident die entsprechende Urabstimmung anzuberaumen.

### §9 Zeitpunkt von Urabstimmungen

1. Urabstimmungen können nur innerhalb der Vorlesungszeit an Vorlesungstagen durchgeführt werden.
2. Der Urabstimmungszeitraum darf nicht in den ersten beiden Wochen und nicht in der letzten Woche der Vorlesungszeit liegen.
3. Urabstimmungen sollen frühestens vier Wochen nach der Durchführung der letzten Urabstimmung stattfinden.

### §10 Zählwert der Stimme

Jede Stimmberechtigte und jeder Stimmberechtigter hat für jeden zur Urabstimmung gestellten Antrag eine Stimme.

### §11 Stimmzettel

1. Die bei der Urabstimmung zu verwendenden Stimmzettel enthalten ausschließlich die im Antrag bzw. Beschluss nach §4 Abs. 3 OrgS festgelegte Fragestellung und die Möglichkeit zur Kennzeichnung der Entscheidung ausschließlich mit „ja“ oder „nein“.
2. Bei zusammengefassten Urabstimmungen sind verschiedene Stimmzettel für die verschiedenen Fragestellungen gemäß Abs. 1 zu verwenden. Die Stimmzettel unterschiedlicher Anträge sollen farblich unterscheidbar sein.

### §12 Ungültige Stimmen

1. Ungültig sind Stimmen, wenn der Stimmzettel
    a) als nicht von der Urabstimmungsleitung hergestellt erkennbar ist,
    b) keine Kennzeichnung enthält,
    c) den Willen der oder des Abstimmenden nicht zweifelsfrei erkennen lässt,
    d) einen Zusatz oder Vorbehalt enthält.
2. Bei der Briefabstimmung gelten mehrere in einem Umschlag enthaltene Stimmzettel als ein Stimmzettel, wenn sie gleich lauten oder nur einer von ihnen gekennzeichnet ist; ansonsten zählen sie als ungültige Stimmen. Ist der Umschlag leer, so gilt dies als ungültige Stimme.
3. Die Stimme einer oder eines Stimmberechtigten, die oder der durch Wahlbrief abgestimmt hat, wird nicht dadurch ungültig, dass sie oder er vor dem oder am Abstimmungstag aus der Studierendenschaft ausscheidet oder ihr oder sein Stimmrecht verliert.

### §13 Zurückzuweisende Wahlbriefe

1. Bei der Briefabstimmung sind Wahlbriefe zurückzuweisen, wenn
    a) der Wahlbrief nicht rechtzeitig eingegangen ist,
    b) dem Wahlbriefumschlag kein oder kein gültiger Stimmschein beiliegt,
    c) weder der Wahlbriefumschlag noch der Wahlumschlag verschlossen sind,
    d) dem Wahlbriefumschlag kein Wahlumschlag beigefügt ist,
    e) der Wahlbriefumschlag mehrere Wahlumschläge, aber nicht eine gleiche Anzahl gültiger Stimmscheine enthält,
    f) kein von der Wahlleitung vorgegebener Wahlumschlag benutzt worden ist,
    g) ein Wahlumschlag benutzt worden ist, der offensichtlich in einer das Abstimmungsgeheimnis gefährdenden Weise von den übrigen abweicht oder einen deutlich fühlbaren Gegenstand enthält,
    h) die Briefwählerin oder der Briefwähler gegen die Briefwahlregelung verstoßen hat und deswegen nicht sichergestellt ist, dass ihr oder sein Stimmzettel ohne vorherige Einsichtnahme in die Abstimmungsurne gebracht werden kann.
2. Die Einsender zurückgewiesener Wahlbriefe werden nicht als Abstimmende gezählt; ihre Stimmen gelten als nicht abgegeben.

### §14 Auszählung

Der Wahlausschuss oder die örtlichen studentischen Beauftragten haben nach Abschluss der Stimmabgabe unverzüglich die abgegebenen Stimmen unter Hinzuziehung von Urabstimmungshelferinnen und Urabstimmungshelfern aus der Studierendenschaft hochschulöffentlich auszuzählen.

### §15 Feststellung des Urabstimmungsergebnisses

1. Der Wahlausschuss stellt auf Grund der Zählergebnisse als Urabstimmungsergebnis gesondert für jeden zur Urabstimmung gestellten Antrag fest:
    a) die Zahl der Abstimmungsberechtigten,
    b) die Zahl der Abstimmenden,
    c) die Zahl der ungültigen Stimmzettel,
    d) die Zahl der gültigen Stimmen,
    e) die Zahl der „ja“- und „nein“-Stimmen,
    f) das Zustandekommen oder das Nichtzustandekommen des Urabstimmungsbeschlusses unter Berücksichtigung von §4 Abs. 4 OrgS,
    g) bei Zustandekommen den entsprechenden Beschluss.
2. Die Urabstimmungsleitung macht das Urabstimmungsergebnis unverzüglich hochschulöffentlich bekannt; dabei ist auf die Möglichkeit hinzuweisen, Einspruch einzulegen, unter Angabe der Einspruchsfrist und der Stelle, bei der Einspruch einzulegen ist.
3. Sofern ein Antrag nach §4 Abs. 2 S. 2 OrgS durch die Urabstimmung angenommen wurde, tritt der entsprechende Beschluss erst am Tag nach der Veröffentlichung in den Amtlichen Mitteilungen der Universität in Kraft. Hierauf ist bei der Bekanntmachung nach Abs. 2 hinzuweisen.

### §16 Bekanntmachungen

1. Der Wahlausschuss beschließt die Form der hochschulöffentlichen Bekanntmachungen der Urabstimmungsleitung. Der Beschluss ist hochschulöffentlich bekannt zu machen.
2. Die hochschulöffentlichen Bekanntmachungen haben mindestens durch geeignete Aushänge in allen Fakultäten und der Zentralmensa, der Nordmensa, der Mensa am Turm und der Mensa Italia zu erfolgen.
3. Bei Aushängen gilt die Bekanntmachung mit dem Tag als bewirkt, an dem der Aushang an den vorgesehenen Aushangstellen erfolgt ist. Beginnend mit diesem Zeitpunkt soll ein vorgeschriebener Aushang mindestens eine Woche dauern. Sind in der Bekanntmachung Einspruchsfristen oder andere Fristen enthalten, darf der Aushang nicht vor Ablauf der Fristen beendet werden. Kurze Unterbrechungen des Aushangs, die nicht durch die Urabstimmungsorgane veranlasst werden, sind bei der Berechnung des Aushangzeitraums nicht zu berücksichtigen.

### §17 Urabstimmungsprüfung

1. Die Urabstimmung kann durch schriftlichen Einspruch, der die Gründe angeben muss, binnen einer Woche nach Bekanntgabe des Urabstimmungsergebnisses angefochten werden. Der Einspruch kann nicht mit der Unrichtigkeit des Wählerverzeichnisses begründet werden. Urabstimmungseinsprüche sind bei der Urabstimmungsleitung einzureichen und mit deren Stellungnahme unverzüglich dem Wahlausschuss zur Entscheidung vorzulegen.
2. Der Wahlausschuss kann jederzeit eine Urabstimmungsprüfung einleiten.
3. Eine Urabstimmung ist zu wiederholen, wenn Verstöße von Wahlrechtsvorschriften sich auf das Abstimmungsergebnis ausgewirkt haben oder ausgewirkt haben können. Wenn eine Wiederholung der Urabstimmung notwendig ist, stellt dies der Wahlausschuss fest.

## Abschnitt II: Wahlparallele Urabstimmungen

### §18 Wählerverzeichnis wahlparalleler Urabstimmungen

Als Wählerverzeichnis wahlparalleler Urabstimmungen gilt das Wählerverzeichnis zur Wahl des Studierendenparlaments. Insbesondere gilt für Mitglieder der Studierendenschaft, die in Studienfächer mehrerer Fakultäten eingeschrieben sind, dass deren Zugehörigkeitserklärung zu einer Fakultät für die Wahl auch für die Urabstimmung gilt.

### §19 Briefwahl bei wahlparallelen Urabstimmungen

Mitgliedern der Studierendenschaft, denen die Universität die Briefwahlunterlagen für die Wahl zum Studierendenparlament aushändigt oder zusendet, lässt sie bei Verfügbarkeit auch die entsprechenden Unterlagen für die wahlparallele Urabstimmung zukommen.

### §20 Durchführung wahlparalleler Urabstimmungen

1. Der Zeitraum einer wahlparallelen Urabstimmung entspricht dem Zeitraum der Wahl des Studierendenparlaments.
2. Die Mitglieder der Studierendenschaft stimmen mit der Wahl des Studierendenparlaments auch über die zur wahlparallelen Urabstimmung gestellten Anträge ab.

## Abschnitt III: Wahlunabhängige Urabstimmungen

### §21 Wählerverzeichnis wahlunabhängiger Urabstimmungen

1. Wahlunabhängige Urabstimmungen werden auf Basis eines Wählerverzeichnisses der Studierendenschaft für das Semester, in dem der Urabstimmungszeitraum liegt, durchgeführt. Im Wählerverzeichnis sind die Mitglieder der Studierendenschaft mit Matrikelnummer, Namen, Vornamen und Fakultätszugehörigkeit aufgeführt.
2. Das Wählerverzeichnis wird auf Basis der Vorlage der Universität erstellt. Die Vorlage für das Wählerverzeichnis wird einmalig für alle wahlunabhängigen Urabstimmungen des jeweiligen Semesters von der Universität innerhalb von zwei Wochen nach Anforderung erstellt. Ist ein Mitglied der Studierendenschaft in Studienfächer mehrerer Fakultäten eingeschrieben, so sind alle diese Fakultäten aufzuführen.
3. Das Wählerverzeichnis wahlunabhängiger Urabstimmungen ist in Ausfertigungen oder Auszügen zusammen mit der Urabstimmungsordnung an mindestens fünf, in der Regel zusammenhängenden, Vorlesungstagen jeweils in einem geschlossenen Zeitraum von mindestens drei Stunden zwischen 10 Uhr und 16 Uhr zur studierendenschaftsöffentlichen Einsichtnahme auszulegen. In der Urabstimmungsbekanntmachung sind die Mitglieder der Studierendenschaft unter Mitteilung des Auslegungszeitraums und des Auslegungsortes zur Einsichtnahme in das Wählerverzeichnis aufzufordern. Der Auslegungszeitraum darf frühestens mit Beginn der Urabstimmungsbekanntmachung beginnen.
4. Ein Mitglied der Studierendenschaft, das in Studienfächer mehrerer Fakultäten eingeschrieben ist, kann während der Einspruchsfrist nach Abs. 5 durch eine Zugehörigkeitserklärung gegenüber der Urabstimmungsleitung bestimmen, in welcher Fakultät es sein Abstimmungsrecht ausüben will; hierauf ist in der Urabstimmungsbekanntmachung hinzuweisen. Liegt nach Ablauf der Einspruchsfrist keine Zugehörigkeitserklärung vor, erfolgt die Zuordnung nach dem ersten Hauptfach.
5. Gegen den Inhalt der Eintragung oder gegen eine Nichteintragung in das Wählerverzeichnis kann jedes Mitglied der Studierendenschaft schriftlich Einspruch bei der Urabstimmungsleitung einlegen. Wird gegen die Eintragung Dritter Einspruch erhoben, sind diese von der Urabstimmungsleitung über den Einspruch zu unterrichten und im weiteren Verfahren zu beteiligen. Die Einspruchsfrist darf frühestens zwei Wochen vor dem Beginn des Urabstimmungszeitraums, aber uhrzeitlich nicht vor Ablauf des Auslegungszeitraums enden und ist in der Urabstimmungsbekanntmachung anzugeben. Legt ein Mitglied der Studierendenschaft wegen einer Eintragung, die es selbst betrifft, Einspruch ein, so kann die Urabstimmungsleitung dem Einspruch durch eine vorläufige Entscheidung abhelfen. Der Wahlausschuss beschließt nach Ablauf der Einspruchsfrist endgültig über die Einsprüche. Die Entscheidungen sind den Einspruch Erhebenden sowie den zu beteiligenden Dritten durch die Urabstimmungsleitung mitzuteilen, wenn die vorläufige Entscheidung der Urabstimmungsleitung, die dem Einspruch abgeholfen hatte, nicht lediglich bestätigt wird.
6. Nach der Entscheidung über die Einsprüche stellt der Wahlausschuss das Wählerverzeichnis fest, spätestens eine Woche vor Beginn des Urabstimmungszeitraums.
7. In das Wählerverzeichnis kann auch nach Beendigung der Auslegungsfrist jedes Mitglied der Studierendenschaft Einblick nehmen.
8. Wiederholungen von Urabstimmungen können aufgrund eines im selben Semester festgestellten Wählerverzeichnisses ohne Auslegung und Einspruchsverfahren stattfinden.
9. Es besteht kein Anspruch auf nachträgliche Eintragung in das Wählerverzeichnis nach Ablauf der Einspruchsfrist.

### §22 Briefwahl bei wahlunabhängigen Urabstimmungen

1. Jedes Mitglied der Studierendenschaft kann bei wahlunabhängigen Urabstimmungen von der Möglichkeit der Briefwahl Gebrauch machen, wenn es das bei der Urabstimmungsleitung in der durch die Wahlbekanntmachung festgesetzten Frist schriftlich beantragt. Die Frist darf frühestens eine Woche vor Beginn des Urabstimmungszeitraums enden. Die Abstimmungsberechtigung ist zu prüfen. Nachdem in das Wählerverzeichnis ein Briefwahlvermerk aufgenommen ist, sind die Briefwahlunterlagen auszuhändigen oder zuzusenden. Briefwahlunterlagen sind
    a) die Stimmzettel mit je einem Stimmzettelumschlag, der den zur Urabstimmung gestellten Antrag erkennen lässt,
    b) der Wahlschein,
    c) der Wahlbrief,
    d) die Briefwahlerläuterung.
2. Briefwählerinnen und Briefwähler tragen die Portokosten.

### §23 Durchführung wahlunabhängiger Urabstimmungen

1. Die Urabstimmungsleitung legt innerhalb des vom Wahlausschuss vorgegebenen Rahmens den Zeitraum der Urabstimmung, die Urabstimmungsbereiche, die Urabstimmungslokale und die Urabstimmungszeiten fest.
2. Die wahlunabhängige Urabstimmung findet innerhalb einer Kalenderwoche an mindestens drei aufeinanderfolgenden Tagen, die zum Vorlesungszeitraum gehören müssen, in der Regel Dienstag bis Donnerstag, statt. An mindestens drei Tagen des Urabstimmungszeitraums muss mindestens in der Zeit von 10 Uhr bis 16 Uhr abgestimmt werden können.
3. Jede Fakultät ist für den gesamten Urabstimmungszeitraum jeweils einem Urabstimmungsbereich zuzuordnen.
4. Die Urabstimmungslokale werden jeweils für mindestens einen Urabstimmungstag eingerichtet.
5. Jedes für einen Urabstimmungstag eingerichtete Urabstimmungslokal ist für den Urabstimmungstag einem Urabstimmungsbereich zuzuordnen. Jedem Urabstimmungsbereich ist an jedem Urabstimmungstag mindestens ein Urabstimmungslokal zuzuordnen.
6. Die Urabstimmungsbereiche und Urabstimmungslokale sind so einzurichten, dass an jeweils mindestens drei Tagen des Urabstimmungszeitraums mindestens ein Urabstimmungslokal in einem der Gebäude am Geisteswissenschaftlichen Zentrum (zentraler Campus), im Universitätsklinikum und in einem der Gebäude im Universitäts-Nordbereich eingerichtet ist.

## Abschnitt IV: Schlussbestimmungen

### §24 Inkrafttreten

Diese Urabstimmungsordnung tritt am Tage nach ihrer Veröffentlichung in den Amtlichen Mitteilungen der Georg-August-Universität Göttingen in Kraft.































