---
title: >-
  Kommunikationsverordnung
date: 2020-08-06
tags: "AStA"
---

Der AStA erlässt folgende Verordnung:

(1) Zur Kommunikation zwischen Organen der Studierendenschaft und von ihnen vertretenen Studierenden, erhebt der AStA E-mail Adressen und Fächerzugehörigkeit aller Studierenden.

(2) Der AStA stellt allen Organen der Studierendenschaft auf Anfrage einen Emailverteiler zur Verfügung über den alle durch das entsprechenden Organ vertretenen Studierenden erreichen werden können.

(3) Die im Rahmen dieser Verordnung erhobenen Daten dürfen ausschließlich für Zwecke nach § 20 Abs. 1 NHG verwendet werden. Bei wiederholtem Verstoß, kann der AStA einen Verteiler aktiv moderieren, also Emails nur nach Prüfung zulassen. Über eine Einschränkung ist das Studierendenparlament, bei Fachschaften und Fachgruppen, die FSRV zu informieren.

II. Inkrafttreten

Abweichend tritt dieser Beschluss mit Beschlussfassung in Kraft.

---

Hinweis: [Der Beschluss kann im AStA-Protokoll von 2020-08-06 nachgelesen werden.](https://asta.uni-goettingen.de/wp-content/uploads/2021/02/protokoll_06_08_20z.pdf)
