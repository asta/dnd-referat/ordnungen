---
title: >-
    Sportordnung der Studierendenschaft der Georg-August-Universität Göttingen (SpO)
date: 2005-09-08
tags: "Sportordnung"
---

^1^Die Studierendenschaft betrachtet es als ihre Aufgabe, den studentischen Sport zu fördern und die sportlichen Interessen ihrer Mitglieder zu unterstützen. ^2^Auf Basis und in Ergänzung zu § 19 OrgS beschließt das Studierendenparlament folgende Ordnung:

## I. Allgemeines
### § 1 Grundsätze
(1)^1^Die Organe des allgemeinen Hochschulsports haben die Aufgabe, den studentischen Sport
zu fördern. ^2^Dabei obliegt ihnen sowohl die Unterstützung des Breitensports als auch die Rege-
lung des Wettkampfsports innerhalb der Studierendenschaft.

(2) Insbesondere haben die Organe des allgemeinen Hochschulsports die Aufgabe:
a) die Studierendenschaft gegenüber den Organen des Allgemeinen Deutschen Hochschulsportverbandes (ADH) zu vertreten,
b) den Sportanteil gemäß § 50 Abs. 3 Lit. a OrgS sowie weitere dem allgemeinen Hochschulsport zugewiesene Mittel im Rahmen einer wirtschaftlichen und sparsamen Haushaltsführung und nach sportförderlichen Gesichtspunkten zu verwalten,
c) universitätsinterne Wettkämpfe sowie Vergleichswettkämpfe mit anderen Hochschulen im In- und Ausland zu veranstalten und die Teilnahme von Göttinger Studierenden an Wettkämpfen anderenorts zu fördern.

### § 2 Organe
Organe des allgemeinen Hochschulsports sind:
a) der Sportausschuss,
b) das Sportreferat,
c) die Obleuteversammlung.

## II. Sportausschuss
### § 3 Aufgaben
(1) Das Studierendenparlament bildet nach Maßgabe von § 15 Abs. 4 OrgS einen Sportausschuss als ständigen Ausschuss.

(2) Der Sportausschuss ist das beschlussfassende Organ des allgemeinen Hochschulsports. Der Sportausschuss beschließt über:
a) den Haushaltsplan des allgemeinen Hochschulsports unter Mitwirkung der Obleuteversammlung,
b) die Verwendung der Mittel entsprechend dem Haushaltsplan des allgemeinen Hochschulsports,
c) die Bildung und Auflösung von Sparten nach § 10,
d) Richtlinien über die Arbeit des Sportreferats.

(3) Die Mitglieder des Sportausschusses sind angehalten, an den Sitzungen der Obleuteversammlung teilzunehmen.

### § 4 Zusammensetzung und Wahl
(1)^1^Der Sportausschuss besteht aus sechs stimmberechtigten Mitgliedern nach Maßgabe von § 15 Abs. 4 OrgS. ^2^Alle Mitglieder dürfen nicht dem AStA angehören.

(2)^1^Drei Mitglieder werden jährlich zu Beginn des Wintersemesters durch die Mehrheit der Mitglieder der Obleuteversammlung gewählt und durch die Präsidentin bzw. ^2^den Präsidenten des Studierendenparlaments ernannt. ^3^Diese müssen Mitglieder der Studierendenschaft sein und müssen nicht dem Studierendenparlament angehören. ^4^Die Mehrheit der Mitglieder des Studierendenparlaments kann die Wahl der Obleuteversammlung zurückweisen. ^5^Eine solche Zurückweisung führt zur Neuwahl.

(3)^1^Drei Mitglieder werden entsprechend § 10 Abs. 4 OrgS be- und ernannt. ^2^Diese sollen nicht zugleich Obleute sein.


(4) Beratende Mitglieder des Sportausschusses sind:
a) die Mitglieder des Sportreferats,
b) die Finanzreferentin oder der Finanzreferent des AStA,
c) eine Vertreterin oder ein Vertreter der zentralen Einrichtung für den allgemeinen Hochschulsport,
d) evtl. Vertreterinnen oder Vertreter der Fraktionen des Studierendenparlaments, welche keinen Sitz im Sportausschuss erhalten.

### § 5 Vorsitz und Geschäftsordnung
(1) Der Sportausschuss wählt eine Vorsitzende oder einen Vorsitzenden sowie eine stellvertretenden Vorsitzende oder einen stellvertretenden Vorsitzenden aus der Mitte seiner stimmberechtigten Mitglieder.

(2) Wird eine Vorsitzende bzw. ein Vorsitzender aus den Reihen der Mitglieder nach § 4 Abs. 2 gewählt, so soll die stellvertretende Vorsitzende bzw. der stellvertretende Vorsitzende den Reihen der Mitglieder nach § 4 Abs. 3 angehören, und umgekehrt.

(3) Die oder der Vorsitzende kann den Sportausschuss jederzeit einberufen. Darüber hinaus tagt der Sportausschuss schnellstmöglich, spätestens innerhalb von 14 Tagen
a) auf Antrag zweier Mitglieder,
b) auf Antrag der Obleuteversammlung,
c) auf Antrag des Sportreferats,
d) auf Antrag des AstA sowie
e) mindestens einmal zu Beginn und zum Ende der Vorlesungszeit.

(4)^1^Die oder der Vorsitzende lädt die Mitglieder des Sportausschusses sowie die beratenden Mitglieder mindestens sieben Tage vor der Sitzung ein. ^2^Die Einladung bedarf der Schriftform. ^3^Die oder der Vorsitzende kündigt die Sitzung spätestens am Tage der Einladung hochschulöffentlich an.

(5)^1^Der Sportausschuss tagt in öffentlicher Sitzung. ^2^Er kann mit Zweidrittelmehrheit die Öffentlichkeit ausschließen oder auf die Hochschulöffentlichkeit oder die Studierendenschaftsöffentlichkeit beschränken, wenn es dringende Belange der Studierendenschaft erfordern.

(6) Über die Sitzungen des Sportausschusses sind Niederschriften anzufertigen.

(7) Die oder der Vorsitzende des Sportausschusses kann im Rahmen von Absatz 4 Aufgaben an das Sportreferat delegieren.

### § 6 Beschlüsse
(1) Der Sportausschuss fasst Beschlüsse über Angelegenheiten nach § 3 Abs. 2 mit der Mehrheit der Mitglieder.

(2) Jedes Mitglied der Studierendenschaft ist berechtigt, Anträge an den Sportausschuss zu stellen.

(3)^1^Beschlüsse sind von der oder dem Vorsitzenden dem Sportreferat zuzuleiten. ^2^Dieses hat die Beschlüsse in jeweils angemessener Form zu veröffentlichen.

## III. Sportreferat
### § 7 Aufgaben
(1)^1^Das Sportreferat ist das vollziehende und mit der Führung der laufenden Geschäfte beauftragte Organ des allgemeinen Hochschulsports. ^2^Es ist mit der Wahrnehmung der Interessen der Studierenden in Angelegenheiten des Hochschulsports beauftragt und vertritt den allgemeinen Hochschulsport.

(2) Das Sportreferat führt seine Geschäfte verantwortlich gegenüber dem Sportausschuss sowie unter Beachtung der Empfehlungen der Obleuteversammlung.

(3) Das Sportreferat arbeitet mit dem AStA zusammen und untersteht seiner Rechtsaufsicht.

(4) Insbesondere nimmt das Sportreferat wahr:
a) die Außenvertretung des allgemeinen Hochschulsports im Rahmen von Tagungen und Veranstaltungen sowie bei Dachverbänden, insbesondere dem ADH,
b) die Zusammenarbeit und Koordination mit der zentralen Einrichtung für den allgemeinen Hochschulsport,
c) die Information der Sparten über Einladungen und Ausschreibungen,
d) die Organisation der Wahl von Obleuten nach § 12,
e) die Information der Studierenden über die Angebote des allgemeinen Hochschulsports,
f) die Information der Obleute über Ausschreibungen und Einladungen zu Wettkämpfen sowie die Förderung der Teilnahme,
g) den Kontakt im Bereich des Sports zu Hochschulen im In- und Ausland,
h) die Organisation von Wettkämpfen,
i) die Umsetzung der Beschlüsse des Sportausschusses.

### § 8 Zusammensetzung und Wahl
(1)^1^Mitglieder des Sportreferates sind die Sportreferentin oder der Sportreferent und die stellvertretenden Sportreferentinnen und Sportreferenten. ^2^Diese dürfen nicht zugleich Obfrau oder Obmann einer Sparte sein. ^3^Sie werden zu Beginn des Wintersemesters einzeln vom Studierendenparlament mit der Mehrheit seiner Mitglieder aus der Mitte der Studierendenschaft auf Vorschlag der Obleuteversammlung auf ein Jahr gewählt. ^4^Die Obleuteversammlung entscheidet über die Anzahl der zu wählenden Stellvertreterinnen und Stellvertreter.

(2) Ein Mitglied scheidet aus dem Sportreferat aus
a) durch Wahl einer Nachfolgerin oder eines Nachfolgers,
b) durch Rücktritt,
c) durch Ausscheiden aus der Studierendenschaft.

### § 9 Stellung der Mitglieder
(1) Die Sportreferentin oder der Sportreferent leitet das Sportreferat und vertritt es nach außen.

(2)^1^Die Mitglieder des Sportreferates sind zur Teilnahme an den Sitzungen des Sportausschusses verpflichtet. ^2^Sind sie an der Teilnahme verhindert, so haben sie sich vor der Sitzung bei der oder dem Vorsitzenden zu entschuldigen.

(3) Die Mitglieder des Sportreferates sind zur Teilnahme an den Sitzungen der Obleuteversammlung verpflichtet.

(4) Die Sportreferentin oder der Sportreferent berichtet dem Sportausschuss und der Obleuteversammlung, auf Verlangen auch dem Studierendenparlament, über die Arbeit des Sportreferats.

(5)^1^Rechtsgeschäftliche Erklärungen müssen durch die Sportreferentin oder den Sportreferenten sowie ein Mitglied des AStA gemeinschaftlich abgegeben werden. ^2^Soll durch sie der allgemeine Hochschulsport verpflichtet werden, so bedürfen sie der Schriftform.

(6) Das Sportreferat kann sich eine Geschäftsordnung geben.


## IV. Sparten & Obleute
### § 10 Sparten
(1) Für die im Rahmen des allgemeinen Hochschulsports ausgeübten Sportarten können jeweils Sparten gebildet werden.

(2) Einer Sparte gehören alle Mitglieder der Studierendenschaft an, die die entsprechende Sportart an der Hochschule ausüben.

(3)^1^Über die Bildung und Auflösung von Sparten entscheidet der Sportausschuss. ^2^Eine Sparte soll gebildet werden, wenn die entsprechende Sportart von wenigstens zehn Studierenden im Rahmen des allgemeinen Hochschulsports ausgeübt wird. ^3^Eine Sparte kann nur dann aufgelöst werden, wenn für sie keine Obfrau bzw. kein Obmann gewählt wurde.

### § 11 Obleute
(1)^1^Die Mitglieder jeder Sparte wählen zum Ende der Vorlesungszeit des Sommersemesters im Rahmen einer Spartenversammlung eine Obfrau oder einen Obmann aus ihrer Mitte. ^2^Die Wahl erfolgt vorbehaltlich der nachfolgenden Bestimmungen auf ein Jahr.

(2) Die Obleute vertreten ihre Sparte.

(3) Obleute scheiden aus ihrem Amt aus:
a) durch Wahl einer Nachfolgerin oder eines Nachfolgers,
b) durch schriftliche Erklärung ihres Rücktritts gegenüber der oder dem Sportreferenten,
c) durch Ausscheiden aus der Studierendenschaft.

(4) Die Durchführung der Spartenversammlung zur Wahl einer Obfrau oder eines Obmannes obliegt – auch im Falle von Abs. 3 Lit. b und c – dem Sportreferat. Dieses achtet auf die Einhaltung der Grundsätze einer freien, gleichen, direkten und geheimen Wahl. Der Termin ist spätestens zehn Tage zuvor hochschulöffentlich anzukündigen. Über Beschwerden entscheidet der Sportausschuss.

(5) Auf Antrag von zehn Mitgliedern oder fünfzig vom Hundert der Mitglieder einer Sparte kann der Sportausschuss für diese Sparte die vorzeitige Neuwahl einer Obfrau oder eines Obmannes nach Abs. 4 beschließen.

(6) Das Sportreferat führt ein Verzeichnis der Sparten sowie der von ihnen gewählten Obleute.

## V. Obleuteversammlung
### § 12 Aufgaben
(1) Die Obleuteversammlung berät den Sportausschuss und das Sportreferat in fachlicher Hinsicht.

(2) Die Obleuteversammlung wirkt an der Verwaltung des allgemeinen Hochschulsports mit, indem sie:
a) drei Mitglieder des Sportausschusses wählt,
b) die Anzahl der stellvertretenden Sportreferentinnen und Sportreferenten feststellt,
c) dem Studierendenparlament Mitglieder der Studierendenschaft zur Wahl zur Sportreferentin
oder zum Sportreferenten bzw. zu stellvertretenden Sportreferentinnen oder stellvertretenden Sportreferenten vorschlägt,
d) dem Sportausschuss einen Haushaltsplan des allgemeinen Hochschulsports zur Verabschiedung vorschlägt.

### § 13 Zusammensetzung
(1) Stimmberechtigte Mitglieder der Obleuteversammlung sind die Obleute der Sparten.

(2) Beratende Mitglieder sind die Mitglieder des Sportausschusses sowie des Sportreferats sowie jeweils eine Vertreterin oder ein Vertreter der zentralen Einrichtung für den allgemeinen Hochschulsport und des AStA.

### § 14 Geschäftsordnung
(1)^1^Die Obleuteversammlung tagt mindestens einmal im Semester. ^2^Darüber hinaus tagt die Obleuteversammlung schnellstmöglich, spätestens innerhalb von zwei Wochen, auf Antrag:
a) eines Fünftels ihrer Mitglieder
b) des Sportausschusses.

(2)^1^Das Sportreferat lädt die Mitglieder der Obleuteversammlung sowie die beratenden Mitglieder und den AStA mindestens sieben Tage vor der Sitzung ein. ^2^Die Einladung bedarf der Schriftform. ^3^Das Sportreferat kündigt die Sitzung spätestens am Tage der Einladung hochschulöffentlich an.

(3)^1^Das Sportreferat schlägt der Obleuteversammlung eine Versammlungsleiterin oder einen Versammlungsleiter aus den Reihen ihrer Mitglieder vor. ^2^Die Obleuteversammlung kann den Vorschlag des Sportreferats zurückweisen und unter Vorsitz der Sportreferentin oder des Sportreferenten eine Versammlungsleiterin oder einen Versammlungsleiter aus ihrer Mitte wählen.

(4)^1^Die Obleuteversammlung tagt in öffentlicher Sitzung. ^2^Sie kann mit Zweidrittelmehrheit die Öffentlichkeit ausschließen oder auf die Hochschulöffentlichkeit oder die Studierendenschaftsöffentlichkeit beschränken, wenn es dringende Belange der Studierendenschaft erfordern.

(5) Über die Sitzungen der Obleuteversammlung sind Niederschriften anzufertigen.

### § 15 Beschlüsse
(1)^1^Die Obleuteversammlung ist beschlussfähig, wenn die Sitzung ordnungsgemäß einberufen wurde und die Mehrheit der stimmberechtigten Mitglieder anwesend ist. ^2^Die Versammlungsleiterin oder der Versammlungsleiter stellt die Beschlussfähigkeit zu Beginn der Sitzung fest. ^3^Die Obleuteversammlung gilt sodann, auch wenn sich die Zahl der anwesenden stimmberechtigten Mitglieder im Laufe der Sitzung verringert, als beschlussfähig, solange nicht ein Mitglied Beschlussunfähigkeit geltend macht. ^4^Dieses Mitglied zählt bei der Feststellung, ob die Obleuteversammlung noch beschlussfähig ist, zu den anwesenden Mitgliedern. ^5^Stellt die Versammlungsleiterin oder der Versammlungsleiter die Beschlussunfähigkeit fest, so beruft sie oder er mindestens zur Behandlung der nicht erledigten Tagesordnungspunkte eine weitere Sitzung ein.

(2)^1^Die Obleuteversammlung fasst Beschlüsse nach § 12 Abs. 2 mit der Mehrheit ihrer Mitglieder. ^2^Im Übrigen entscheidet sie mit der Mehrheit der Anwesenden.

(3) Jedes Mitglied der Studierendenschaft ist berechtigt, Anträge an die Obleuteversammlung zu stellen.

(4)^1^Beschlüsse und Empfehlungen sind von der Versammlungsleiterin oder dem Versammlungsleiter dem Sportreferat zuzuleiten. ^2^Dieses hat sie in jeweils angemessener Form zu veröffentlichen.

## VI. Finanzen
### § 16 Grundsätze
(1)^1^Der allgemeine Hochschulsport erhält zur Erfüllung seiner Aufgaben Mittel gemäß den Regelungen der Beitragsordnung der Studierendenschaft. ^2^Ihm können weitere Mittel, auch dritter Hand, zugewiesen werden.

(2) Das Verfügungsrecht über die Mittel nach Abs. 1 liegt beim Sportausschuss, soweit nicht anderes bestimmt ist.

(3) Die Bestimmungen der Finanzordnung der Studierendenschaft gelten sinngemäß.

### § 17 Haushaltsplan
(1)^1^Zu Beginn des Haushaltsjahres entwirft das Sportreferat einen Haushaltsplan zur Vorlage in der Obleuteversammlung. ^2^Einzelne Titel können nach Sparten und Semestern gegliedert werden, sofern dies sachlich angemessen ist sowie Übersichtlichkeit und Handhabbarkeit des Haushalts erhöht.

(2) ^1^Die Obleuteversammlung berät über den Haushaltsplan und überweist ihn an den Sportausschuss. ^2^Der Sportausschuss stellt den Haushaltsplan in der vorgeschlagenen Form fest oder überweist ihn zurück an die Obleuteversammlung. ^3^Der Finanzreferentin oder dem Finanzreferenten des AStA ist vor der Feststellung Gelegenheit zur Stellungnahme einzuräumen.

(3)^1^Der Haushaltsplan muss sachlich und inhaltlich deutlich getrennt sein von anderen im Rahmen des Hochschulsports aktiven Personen, Vereinen oder Institutionen. ^2^Sofern Mittel gemeinsam verwaltet werden, sind sie und ihre Verwendung gesondert auszuweisen.

(4) Der Haushaltsplan muss zumindest folgende Ausgabentitel aufweisen:
a) Anschaffungen, Ersatz & Ergänzung von Geräten
b) Aufwandsentschädigungen
c) Sportveranstaltungen in Göttingen
d) auswärtige Sportveranstaltungen
e) Geschäftsbedarf
f) Reisekosten.

(5)^1^Die Höhe der Aufwandsentschädigung für Mitglieder des Sportreferats regelt der Sportausschuss. ^2^Diese soll die Aufwandsentschädigung, welche Mitgliedern des AStA gewährt wird, nicht überschreiten.

### § 18 Ausführung des Haushalts
^1^Feststellungsbefugt im Sinne des § 19 FinO ist die Sportreferentin oder der Sportreferent. ^2^Anordnungsbefugt ist die Finanzreferentin oder der Finanzreferent des AStA. ^3^Jede Auszahlung bedarf eines entsprechenden Beschlusses des Sportausschusses.

## VII. Schlussbestimmungen
### § 19 Inkrafttreten & Übergangsbestimmungen
(1)^1^Diese Ordnung tritt am Tage nach ihrer Veröffentlichung in den Amtlichen Mitteilungen der Georg-August-Universität Göttingen in Kraft. ^2^Gleichzeitig tritt die bestehende Satzung für den allgemeinen Hochschulsport nebst ihren Ergänzungsordnungen außer Kraft.

(2) Die nach Maßgabe der Satzung für den allgemeinen Hochschulsport gebildeten Organe bleiben bis zum Ende ihrer Legislaturperiode bestehen.

(3) Zum Beginn des Sommersemesters 2006 werden die Mitglieder des Sportreferats entsprechend den Bestimmungen dieser Ordnung, jedoch auf ein Semester gewählt.

(4)^1^Die bei Inkrafttreten dieser Ordnung bestehenden Sparten bedürfen der Bestätigung des Sportausschusses gemäß den Bestimmungen dieser Ordnung über die Bildung und Auflösung von Sparten. ^2^Sparten, die nicht entsprechend Satz 1 bis zum 30. September 2006 bestätigt werden, gelten zum 01. Oktober 2006 als aufgelöst.

