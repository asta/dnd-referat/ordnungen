---
title: Wahlordnung der Studierendenschaft der Georg-August-Universität Göttingen (WO)
date: 2020-04-10
tags:
  - Wahlordnung
  - WO
  - Studierendenschaft
fassung_der_bekanntmachung:
  date: 2020-11-02
  ort: Amtliche Mitteilungen I 64/2020 S. 1336 ff.
zuletzt_geaendert:
  gremium: Studierendenparlament
  date: 2021-03-17
  ort: Amtliche Mitteilungen I 18/2021 S. 281
---

Wahlordnung der Studierendenschaft der Georg-August-Universität Göttingen (WO) in der Fassung der Bekanntmachung vom 02.11.2020 (Amtliche Mitteilungen I 64/2020 S. 1336 ff.), zuletzt geändert durch Beschluss des Studierendenparlaments vom 17.03.2021 (Amtliche Mitteilungen I 18/2021 S. 281)


# Wahlordnung der Studierendenschaft der Georg-August-Universität Göttingen (WO)

## §1 Geltungsbereich

1. Die Vorschriften dieser Ordnung gelten für die Wahlen zu den folgenden Organen der Studierendenschaft:Studierendenparlament, Ausländisches Studierendenparlament, Fachschaftsparlamente, Fachgruppensprecher\*innen.
2. Die Wahlen sollen als verbundene Wahlen gleichzeitig vorbereitet und durchgeführt werden. Der Wahlzeitraum soll in der Vorlesungszeit des Wintersemesters liegen und zu Beginn des Wintersemesters festgelegt werden; als Vorlesungszeit gilt der durch das Präsidium der Universität (im Folgenden: Präsidium) beschlossene Vorlesungszeitraum.
3. Die Wahlen werden als Urnenwahl mit Briefwahlmöglichkeit durchgeführt. Die Wahlleitung kann im Einvernehmen mit dem Wahlausschuss festlegen, dass die Wahl abweichend von Satz 1 als internetbasierte Onlinewahl (digitale Wahl) mit Briefwahlmöglichkeit durchgeführt wird.

## §2 Wahlausschuss

1. Der Wahlausschuss überwacht die Ordnungsmäßigkeit der Wahlen der Organe der Studierendenschaft und ist für diese Wahlen in Zusammenarbeit mit der Wahlleitung verantwortlich. Er ist zuständig, soweit es diese Ordnung festlegt.
2. Der Wahlausschuss hat sieben Mitglieder. Den Wahlausschuss bilden Mitglieder der Studierendenschaft, die gemäß §10 Abs. 4 OrgS benannt werden. Die Wahlleitung und die\*der Präsident\*in des Studierendenparlaments können mit beratender Stimme teilnehmen. Die FSRV kann eine Vertretung benennen, die mit beratender Stimme teilnehmen kann.
3. (Entfällt)
4. Die Amtszeit der Mitglieder des Wahlausschusses beginnt mit dem Wintersemester und endet nach einem Jahr. Scheidet ein Mitglied des Wahlausschusses vorzeitig aus und ist eine Stellvertretung nicht mehr vorhanden, so werden für den Rest der Amtszeit ein neues Mitglied und eine Stellvertretung nachbenannt.
5. Die Wahlleitung lädt zur ersten Sitzung des Wahlausschusses ein und leitet sie, bis der Wahlausschuss aus seiner Mitte eine\*einen Vorsitzende\*n beziehungsweise, sofern erforderlich, bis der Wahlausschuss eine\*einen stellvertretende\*n Vorsitzende\*n benannt hat. Die\*der Vorsitzende des Wahlausschusses lädt zu den Sitzungen ein und leitet sie. Die Person ist zur Einberufung des Wahlausschusses verpflichtet, wenn dies das Präsidium des Studierendenparlaments (im Folgenden: StuPa-Präsidium), der AStA, wenigstens drei Mitglieder des Wahlausschusses oder die Wahlleitung fordern.
6. Die Wahlleitung bestellt für die Koordinierung der Aufgaben in den Wahllokalen eine\*einen Wahlkoordinator\*in und jeweils wenigstens eine Stellvertretung, die im Falle eines fakultären Wahllokals durch den Fachschaftsrat vorzuschlagen sind.
7. Die Wahlleitung und die Wahlkoordinator\*innen können für die Beaufsichtigung der Wahlhandlung sowie für die Auszählung Wahlhelfende bestellen. Alle Gliederungen der Studierendenschaft sind verpflichtet, Wahlhelfende zu benennen.
8. (Entfällt)

## §3 Sitzungen und Beschlüsse des Wahlausschusses

1. Die Einladung muss spätestens eine Woche vor der Sitzung des Wahlausschusses in Textform versandt werden.
2. Der Wahlausschuss ist beschlussfähig, wenn nach ordnungsgemäßer Einberufung die Mehrheit der stimmberechtigten Mitglieder anwesend ist. (Satz 2 entfällt)
3. Eine Beschlussfassung im Umlaufverfahren ist in folgenden Fällen unzulässig, sofern in dieser Ordnung nicht etwas anderes bestimmt ist:
    a) Zulassung oder Nichtzulassung von Wahlvorschlägen;
    b) Feststellung des Wahlergebnisses;
    c) soweit eine persönliche Abstimmung vorgeschrieben ist.


## §4 Wahlleitung

1. Die Wahlleitung obliegt einem Mitglied der Universität, benannt durch das Studierendenparlament mit einfacher Mehrheit. Sie ist für die ordnungsgemäße Vorbereitung und Durchführung der Wahl verantwortlich. Sie muss sich mit der Benennung einverstanden erklären. Erfolgt diese Erklärung nicht oder erfolgt keine Benennung, obliegt die Wahlleitung dem Präsidium des Studierendenparlamentes, welches seine Entscheidungen gemäß §8 Abs. 4 S. 3 und S. 4 OrgS. fällt. Obliegt dem StuPa-Präsidium die Wahlleitung kann in dringenden Fällen die\*der Präsident\*in des Studierendenparlaments für diese Funktion allein Entscheidungen treffen, die vom StuPa-Präsidium oder dem Wahlausschuss jederzeit per Beschluss für die Zukunft aufgehoben werden können. Kann nach §1 Abs. 3 S. 2 kein Einvernehmen hergestellt werden, hat die Wahlleitung die Möglichkeit ihr Einverständnis gemäß Satz 3 zurückzuziehen.
2. Die Wahlleitung hat das Recht und die Pflicht, an den Sitzungen des Wahlausschusses teilzunehmen. Die Wahlleitung hat die Sitzungen des Wahlausschusses mit dessen Vorsitzender\*Vorsitzendem vorzubereiten, Entscheidungsvorschläge vorzulegen, sowie das Protokoll fertigen zu lassen und für die Bekanntmachung und Durchführung der Beschlüsse zu sorgen. Die Wahlleitung legt den Zeitplan für die Wahlvorbereitung mit den Einsichtnahme-, Einspruchs- und Einreichungsfristen nach Stellungnahme des Wahlausschusses fest. Die digitale Wahl setzt voraus, dass bei ihrer Durchführung die geltenden Wahlrechtsgrundsätze, insbesondere die Grundsätze der freien, gleichen und geheimen Wahl und der Öffentlichkeit der Wahl, gewahrt werden.
3. Falls die Wahlleitung hierzu befugt ist, kann diese zur Durchführung ihrer Aufgaben die Beschäftigten der Universität heranziehen und Aufgaben vollumfänglich an diese delegieren, einschließlich der Entgegennahme der Stimmzettel nach §17 Abs. 4.
4. Die Wahlleitung ist berechtigt, jederzeit Fehler, Unstimmigkeiten oder Schreibversehen (im Folgenden insgesamt: Fehler) in Wahlvorschlägen, Wahlverzeichnissen, Bekanntmachungen, Wahlausschreibung oder Ergebnissen zu berichtigen; dies gilt nicht für die Feststellung des Wahlergebnisses, sofern sich durch die Berichtigung die Zuordnung der Sitze an eine\*einen Bewerber\*in ändert und der Fehler nicht offensichtlich ist. Die Berichtigung ist aktenkundig zu machen und hierbei mit Datum und Namenszeichen zu versehen.

## §5 Wahlbereiche

1. Alle Mitglieder der Studierendenschaft, die für dasselbe Organ wahlberechtigt sind, bilden für dessen Wahl einen Wahlbereich. Ein Wahlbereich ist in Wahlteilbereiche untergliedert; sofern die Wahlleitung nichts anderes bestimmt, bildet eine Fachschaft einen Wahlteilbereich.
2. Wahlvorschläge können sich nur auf einen Wahlbereich beziehen. In diesem Wahlbereich muss jede\*jeder Bewerber\*in eines Wahlvorschlags wahlberechtigt sein.

## §6 Aufstellung des Wahlverzeichnisses

1. Wählen und gewählt werden darf nur, wer in das Wahlverzeichnis eingetragen ist. Wer Mitglied mehrerer Fachgruppen oder Fachschaften ist, darf sein Wahlrecht nur innerhalb einer Fachgruppe oder einer Fachschaft ausüben, soweit nicht etwas Abweichendes bestimmt ist.
2. Die Wahlleitung hat zu Beginn der Vorlesungszeit des Semesters, in dem eine Wahl stattfindet, alle Personen, die nach dem Gesetz oder OrgS zu dieser Wahl wahlberechtigt sind, in ein Wahlverzeichnis eintragen zu lassen. (Satz 2 entfällt)
3. Aus dem Wahlverzeichnis müssen sich für jede wahlberechtigte Person der Wahlbereich, die Fachschaft sowie die Fachgruppenzugehörigkeit und die Zugehörigkeit zur ausländischen Studierendenschaft ergeben. Mitglieder der Studierendenschaft, die keiner Fachschaft zuzuordnen sind, werden gesondert aufgeführt. Das Wahlverzeichnis muss den Familien- und Vornamen der Wahlberechtigten nennen. Weitere Angaben (z.B. Geburtsdatum, Anschrift, Matrikelnummer oder, Studiengang) sind aufzuführen, wenn dies erforderlich ist, um Verwechslungen auszuschließen oder die Wahlberechtigung für ein Organ sicher feststellen zu können.
4. Wer Mitglied mehrerer Untergliederungen (z.B. Fachschaften, Fachgruppen) ist, kann durch eine Zugehörigkeitserklärung gegenüber der Wahlleitung bestimmen, in welcher Untergliederung das Wahlrecht ausgeübt werden soll. Eine Kandidatur gilt in solchen Fällen als Zugehörigkeitserklärung. Die Wahlleitung kann unter Fristsetzung zur Abgabe einer Zugehörigkeitserklärung auffordern. Liegt nach Ablauf der Frist eine Zugehörigkeitserklärung nicht vor, kann die Wahlleitung die Zuordnung nach ihrem Ermessen vornehmen; Entsprechendes gilt, wenn eine Aufforderung nach Satz 3 nicht ergangen ist. Ein Antrag auf nachträgliche Eintragung (§7) gilt als Zugehörigkeitserklärung. Die Sätze 1-5 gelten entsprechend, sofern diese Wahlordnung für die Durchführung einer anderen Wahl gilt.
5. Das Wahlverzeichnis ist in Ausfertigungen oder Auszügen zusammen mit dem Text der Wahlordnung mindestens an einer Stelle am Sitz der Universität unter Beachtung der datenschutzrechtlichen Bestimmungen zur Einsichtnahme oder gegen Nachweis der Mitgliedschaft digital zur Einsichtnahme bereitzustellen. In der Wahlausschreibung sind die Wahlberechtigten unter Mitteilung, in welcher Art und in welchem Zeitraum die Einsichtnahme möglich ist, zur Einsichtnahme in das Wahlverzeichnis aufzufordern. Der Zeitraum für die Möglichkeit zur Einsichtnahme (Einsichtnahmefrist) muss mindestens die Woche nach Bekanntgabe der Wahlausschreibung umfassen.
6. Gegen den Inhalt der Eintragung oder gegen eine Nichteintragung in das Wahlverzeichnis kann jede wahlberechtigte Person schriftlich Einspruch bei der Wahlleitung einlegen. Wird gegen die Eintragung Dritter Einspruch erhoben, sind diese von der Wahlleitung über den Einspruch zu unterrichten und im weiteren Verfahren zu beteiligen, sofern das Wahlverzeichnis auf den Einspruch hin geändert werden soll. Die Einspruchsfrist (Ausschlussfrist) darf frühestens acht Wochen vor dem ersten Tag des Wahlzeitraums, aber nicht vor Ablauf der Einsichtnahmefrist enden und ist in der Wahlausschreibung bekannt zu geben; nach Ablauf der Einspruchsfrist ist ein Einspruch gegen das Wahlverzeichnis ausgeschlossen. Die Entscheidung über den Einspruch trifft die Wahlleitung nach Stellungnahme des Wahlausschusses spätestens am zehnten Vorlesungstag nach Ablauf der Einspruchsfrist.
7. Nach der Entscheidung über die Einsprüche und nach Stellungnahme des Wahlausschusses stellt die Wahlleitung das vorläufige Wahlverzeichnis fest, das die maßgebliche Grundlage für die Entscheidung über die Zulassung der Wahlvorschläge und die Berechnung der Sitze eines Organs ist, unabhängig davon, ob das endgültige Wahlverzeichnis hiervon abweicht. Wer Mitglied der Studierendenschaft nach Ablauf der Einspruchsfrist wird, ist nicht wählbar.
8. In das Wahlverzeichnis kann auch nach Beendigung der Einsichtnahmefrist jedes Mitglied der Studierendenschaft oder von der Wahlleitung Befugte Einblick nehmen.
9. Nach-, Ergänzungs- und Neuwahlen können auf Grund eines im selben Semester festgestellten Wahlverzeichnisses ohne erneute Möglichkeit zur Einsichtnahme und Einspruchsverfahren stattfinden. Aktualisierungen nach §7 bleiben möglich.

## §7 Feststellung des endgültigen Wahlverzeichnisses

1. Die Wahlleitung soll das Wahlverzeichnis bis zum vierzehnten Tag vor Beginn des Wahlzeitraums aktualisieren; ein Anspruch hierauf besteht nicht. Feststellungen des Wahlausschusses oder der Wahlleitung, die vor der Aktualisierung liegen, bleiben hiervon unberührt. Nach Ablauf der Frist nach Satz 1 stellt die Wahlleitung das endgültige Wahlverzeichnis für die Ausübung des aktiven Wahlrechts fest. Wer nach Ablauf dieser Frist Mitglied der Studierendenschaft wird, ist nicht wahlberechtigt. Die Aktualisierung kann auch die Änderung der Fachgruppen- oder Fachschaftszugehörigkeit betreffen. Im Falle einer digitalen Wahl wird aus dem Wahlverzeichnis, das auf einem universitären Server (einschließlich Server der GWDG) gespeichert ist, durch Verschlüsselung in Hashwerte ein digitales Wahlverzeichnis im digitalen Wahlsystem generiert.
2. Endet oder ruht nach Feststellung des vorläufigen Wahlverzeichnisses die Mitgliedschaft für einen Wahlbereich, verliert die oder der Betroffene hierdurch abweichend von Absatz 1 das Wahlrecht. (Rest entfällt)
3. Im Falle einer nachträglichen Eintragung kann die Wahlleitung der oder dem betreffenden Wahlberechtigten einen Wahlschein erteilen, wenn das für den Nachweis der Wahlberechtigung bei der Abstimmung zweckmäßig ist.

## §8 Wahlbenachrichtigung

Über die Eintragung in das Wahlverzeichnis erhält die wahlberechtigte Person spätestens drei Wochen vor der Wahl eine Wahlbenachrichtigung möglichst auf digitalem Wege. Die Wahlbenachrichtigung enthält das Antragsformular für die Beantragung der Briefwahl.

## §9 Wahlausschreibung

1. Die Wahlleitung hat die Wahl durch eine Wahlausschreibung öffentlich bekannt zu machen. Die Wahlausschreibung muss angeben:
    1. das Wahlverfahren,
    2. die zu wählenden Organe,
    3. den vom Wahlausschuss im Einvernehmen mit der Wahlleitung festgelegten Wahlzeitraum, die Aufforderung zur Einsichtnahme in das Wahlverzeichnis nach §6 Abs. 5 mit dem Hinweis auf die Möglichkeit, Einspruch einzulegen, auf die Einspruchsfrist sowie auf Ort und Zeit für die Abgabe von Einsprüchen,
    4. die Frist für nachträgliche Eintragungen,
    5. die Aufforderung zur Einreichung von Wahlvorschlägen nach §10 Abs. 2 unter Angabe
        a) der Gesamtzahl der Sitze des Organs, sofern möglich
        b) der Wahlbereiche,
        c) der Einreichungsfrist und –form und
        d) von Ort und Zeit für die Einreichung von Wahlvorschlägen,
    6. die Regelungen für die Briefwahl mit Angabe der Frist für Briefwahlanträge.
2. Mit der Wahlausschreibung können andere öffentliche Bekanntmachungen verbunden werden, insbesondere
    1. die Mitteilung, in welchen Wahlbereichen eine Wahl voraussichtlich entfällt, weil die Zahl der Wahlberechtigten die Gesamtzahl der Sitze im Organ nicht übersteigt,
    2. die Form öffentlicher Bekanntmachungen nach §22.
3. Die Wahlausschreibung kann in Teilen nacheinander veröffentlicht werden. Alle nach Abs. 1 notwendigen Bekanntmachungen sollen fünf Wochen vor dem ersten Tag des Wahlzeitraums öffentlich bekannt gemacht sein.

## §10 Einreichung von Wahlvorschlägen

1. Der Wahl liegen Wahlvorschläge zugrunde, die mehrere Bewerber\*innen (Listenwahlvorschläge) oder eine\*einen Bewerber\*in (Einzelwahlvorschläge) benennen können. Jeder Wahlvorschlag darf sich nur auf die Wahl eines Organs beziehen.
2. Die Wahlvorschläge sind bei der Wahlleitung einzureichen. Die Einreichungsfrist (Ausschlussfrist) darf nicht früher als eine Woche nach Bekanntmachung der Wahlausschreibung und nicht später als zwei Wochen vor dem ersten Tag des Wahlzeitraums enden.
3. Die Bewerber\*innen müssen zu den Organen, zu denen sie aufgestellt sind, passiv wahlberechtigt sein. Die Wahlberechtigung kann nur durch das festgestellte Wahlverzeichnis (im Falle der digitalen Wahl einschließlich des digitalen Wahlverzeichnis) nachgewiesen werden. Jede\*jeder Bewerber\*in darf für die Wahl desselben Organs nur auf je einem Wahlvorschlag benannt werden. Die Bewerbung einer\*eines mit ihrem\*seinem Einverständnis auf mehreren Wahlvorschlägen genannten Bewerber\*in gilt nur für den von ihr\*ihm bis zum Ablauf der Einreichungsfrist bezeichneten Wahlvorschlag, sonst für den zuletzt eingereichten Wahlvorschlag oder den durch gesonderte Willenserklärung genauer bezeichneten Wahlvorschlag; bei gleichzeitigem Eingang der Wahlvorschläge entscheidet das Los entsprechend §14 Abs. 2 Satz 2.
4. Der Wahlvorschlag muss die Bewerber\*innen in einer deutlichen Reihenfolge mit Namen, Vornamen, Geburtsdatum, Anschrift, universitärer E-Mail-Adresse, Fachschaftszugehörigkeit oder Matrikelnummer aufführen. Freiwillige Angaben (z. B. Amtsbezeichnung, Titel, Studiengang, ausgeübte Tätigkeit) können im Umfang von bis zu 250 Zeichen (einschließlich Leerzeichen) hinzugefügt werden; sie sind auf Anforderung der Wahlleitung auch noch nach Zulassung des Wahlvorschlags hinzuzufügen, wenn das notwendig ist, um Verwechslungen zu verhindern. Sofern freiwillige Angaben einer Bewerber\*in im Wahlvorschlag enthalten sind, sollen diese an der entsprechenden Stelle in die Wahlbekanntmachung aufgenommen werden; dies gilt nicht für Kontaktdaten (z.B. Telefonnummer, Adresse). Dem Wahlvorschlag muss eine Erklärung jeder\*jedes Bewerberin\*Bewerbers dieses Wahlvorschlags beigefügt sein, dass die jeweilige Bewerber\*in mit der Kandidatur und dem sie oder ihn betreffenden Angaben einverstanden ist und für den Fall ihrer\* seiner Wahl diese annehmen wird. Es kann ein Kennwort angegeben werden, unter dem der Wahlvorschlag in der Wahlbekanntmachung und auf dem Stimmzettel geführt werden soll.
5. In jedem Wahlvorschlag soll eine Vertrauensperson unter Angabe ihrer Anschrift und möglichst auch ihrer Telefonnummer und E-Mail-Adresse benannt werden. Diese muss Mitglied der Studierendenschaft, nicht aber selbst Bewerber\*in sein. Falls keine besondere Benennung erfolgt, gilt die\*der Übersender\*in des Wahlvorschlags, sonst die\*der in der Reihenfolge an erster Stelle genannte Bewerber\*in als Vertrauensperson des Wahlvorschlags. Die Vertrauensperson muss den eingereichten Wahlvorschlag unterzeichnen und bestätigt dadurch die Übereinstimmung der schriftlichen mit der digitalen Fassung des Wahlvorschlages. Die Vertrauensperson ist als Vertretung aller Bewerberinnen oder Bewerber zur Abgabe und zum Empfang von Erklärungen gegenüber den Wahlorganen berechtigt und verpflichtet. Neben ihr sind die einzelnen Bewerber\*innen zur Abgabe und zum Empfang von Erklärungen gegenüber den Wahlorganen berechtigt, soweit nur sie selbst betroffen sind.
6. Das Wahlvorschlagsformular ist bis zum Ende der Einreichungsfrist zusätzlich zu der schriftlichen Form (unterschriebener Ausdruck) auf digitalem Wege per E-Mail oder auf einem körperlichen Datenträger, z.B. CD-Rom, DVD, USB-Stick, einzureichen. Für die Erstellung des Wahlvorschlags sind ausschließlich das von der Wahlleitung für die Wahl zugelassene Wahlvorschlagsformular und das von der Wahlleitung für die Wahl zugelassene Einverständniserklärungsformular zu verwenden. Diese Formulare werden von der Wahlleitung in geeigneter Weise zur Verfügung gestellt.
7. Jede\*jeder Wahlberechtigte hat das Recht, eingegangene Wahlvorschläge zu den in der Wahlausschreibung festgelegten Zeiten bei der von der Wahlleitung bestimmten Stelle einzusehen.
8. Die Wahlleitung kann im Einvernehmen mit dem Wahlausschuss festlegen, dass Wahlvorschläge ausschließlich digital einzureichen sind oder ausschließlich digital eingereicht werden können und in welcher Form (z. B. über ein Portal) dies zu erfolgen hat. Die Formvorgaben sind in der Wahlausschreibung bekannt zu machen. 3 Ist die digitale Einreichung während der Einreichungsfrist aus von der Universität zu vertretenden technischen Gründen nicht möglich, kann die Wahlleitung die Einreichungsfrist angemessen verlängern und hierüber im Internet informieren; tritt die von der Universität zu vertretende Störung am letzten Tag der Einreichungsfrist auf, verlängert sich die Einreichungsfrist um einen Tag.

## §11 Zulassung der Wahlvorschläge

1. Die Wahlleitung vermerkt für jeden eingereichten Wahlvorschlag Tag und Uhrzeit des Eingangs. Sie prüft für den Wahlausschuss, ob die Wahlvorschläge frist- und formgerecht eingereicht wurden; sie ist nicht verpflichtet, die Angaben zugunsten der Bewerber\*innen von Amts wegen zu überprüfen. Bis zum Ablauf der Einreichungsfrist können Wahlvorschläge zurückgenommen, geändert oder ergänzt werden.
2. Der Wahlausschuss soll spätestens am fünfzehnten Vorlesungstag nach Ablauf der Einreichungsfrist über die Zulassung oder Nichtzulassung der Wahlvorschläge entscheiden.
3. Nicht zuzulassen sind Wahlvorschläge, die
    1. nicht oder nicht vollständig bis zum festgesetzten Termin eingereicht sind,
    2. nicht erkennen lassen, für welche Wahl oder für welchen Wahlbereich sie bestimmt sind,
    3. die Bewerber\*innen nicht eindeutig bezeichnen,
    4. die Einverständniserklärungen der Bewerber\*innen nicht enthalten,
    5. Bewerber\*innen aufführen, die nach dem festgestellten Wahlverzeichnis im betreffenden Bereich nicht wählbar sind,
    6. Bedingungen oder Einschränkungen enthalten.

    Soweit die Nichtzulassungsgründe sich nur auf einzelne Bewerber\*innen eines Listenwahlvorschlags beziehen, sind nur diese nicht zuzulassen und aus dem Wahlvorschlag zu streichen. Zur Fristwahrung reicht die Übersendung der vollständigen Unterlagen auf digitalem Wege oder per Fax aus, wenn die Unterlagen unverzüglich, spätestens aber am zweiten Vorlesungstag nach Fristablauf, im Original nachgereicht werden.
4. Lässt der Wahlausschuss einen Wahlvorschlag ganz oder teilweise nicht zu, so hat die Wahlleitung unverzüglich die Vertrauensperson dieses Wahlvorschlags unter Angabe der Gründe zu unterrichten.
5. Geben die Kennwörter mehrerer Wahlvorschläge Anlass zu Verwechslungen, so ist der Wahlausschuss nach Stellungnahme der Vertrauenspersonen der betroffenen Wahlvorschläge, berechtigt, das Kennwort eines Wahlvorschlags um eine Unterscheidungsbezeichnung zu ergänzen. Bei der Abwägung der widerstreitenden Interessen ist das berechtigte Interesse an der fortgesetzten Verwendung des Kennworts besonders zu berücksichtigen; die Unterscheidungsbezeichnung soll dem Wahlvorschlag beigefügt werden, der auch unter Berücksichtigung früherer Wahlen – später als der konkurrierende Wahlvorschlag in Erscheinung getreten ist. Ob ein überwiegendes Interesse besteht, ist danach zu beurteilen, ob die Kandidat\*innen eines Wahlvorschlags bereits im Vorjahr unwidersprochen unter demselben Kennwort angetreten sind, sodann ob auf einem Wahlvorschlag die Anzahl der Kandidat\*innen überwiegt, die bereits in den Vorjahren unter demselben Kennwort angetreten sind. Lässt sich das überwiegende berechtigte Interesse eines Wahlvorschlags nicht mit zumutbarem Aufwand feststellen, entscheidet das Los.

## §12 Entscheidung der Wahlorgane für die Wahlbekanntmachung

1. Auf Grund des festgestellten Wahlverzeichnisses hat die Wahlleitung endgültig festzustellen, ob für einen Wahlbereich nicht mehr wählbare Mitglieder oder zugelassene Bewerberinnen oder Bewerber vorhanden sind als die Gesamtsitzzahl des Organs, so dass eine Wahl entfällt.
2. Liegen für ein Organ nur Einzelwahlvorschläge oder nur ein Listenwahlvorschlag vor, so hat die Wahlleitung festzustellen, dass in dem betroffenen Organ nach den Grundsätzen der Mehrheitswahl zu wählen ist. Nach den Grundsätzen der Mehrheitswahl wird ferner gemäß Feststellung der Wahlleitung gewählt, wenn ein Gremium nur aus einem Mitglied besteht oder nur ein Mitglied eines Wahlbereichs zu wählen ist. In allen anderen Fällen findet Listenwahl statt.
3. Die Wahlleitung legt nach Stellungnahme des Wahlausschusses für die einzelnen Wahlbereiche oder Wahlteilbereiche die Wahlräume und die Tageszeiten fest, zu denen während des Wahlzeitraums die Stimmabgabe möglich ist. Im Falle einer digitalen Wahl legen die Wahlleitung und der Wahlausschuss einvernehmlich die Internetadresse des Wahlportals sowie den Beginn und das Ende des Wahlzeitraumes (Beginn und Schluss der Möglichkeit zur Stimmabgabe) fest. Die Wahlleitung legt wenigstens einen Ort fest, an dem die Stimmabgabe in digitaler Form während der in der Wahlausschreibung festgelegten Dienstzeiten unter Verwendung eines durch die Universität bereitgestellten Computers möglich ist.
4. Die Wahlleitung hat durch einen Nachtrag zur Wahlausschreibung diese ganz oder teilweise zu wiederholen, insbesondere erneut zur Einreichung von Wahlvorschlägen für einzelne Wahlbereiche aufzufordern, wenn
    1. die Zahl der Bewerber\*innen aller Wahlvorschläge zu einem Wahlbereich die Gesamtzahl der Sitze dieses Organs unterschreitet oder
    2. sonst eine Nachwahl nach §20 Abs. 1 notwendig würde und hierfür nach Feststellung des Wahlausschusses neue Wahlvorschläge eingereicht werden dürfen.

    Die bisher eingereichten zugelassenen Wahlvorschläge brauchen nicht nochmals eingereicht werden, können aber innerhalb der neuen Wahlvorschlagsfrist geändert werden. Im Falle des Satzes 1 Nr. 1 ist nur einmal durch einen Nachtrag zur Wahlausschreibung erneut zur Einreichung von Wahlvorschlägen aufzufordern. Im Falle einer Nachfrist ist die Wahlleitung zuständig für die Entscheidung über die Zulassung von Wahlvorschlägen; gegen eine ablehnende Entscheidung kann die betroffene Person innerhalb von zwei Tagen nach Bekanntgabe der Entscheidung Einspruch bei der Wahlleitung einlegen, über den der Wahlausschuss innerhalb von fünf Vorlesungstagen zu entscheiden hat.
    
## §13 Wahlbekanntmachung

1. Die Wahlleitung veröffentlicht in der Wahlbekanntmachung
    1. die Aufforderung zur Stimmabgabe mit dem Hinweis a) auf den Wahlzeitraum, die Wahlräume und auf die Tageszeiten für die Stimmabgabe bzw. b) im Falle einer digitalen Wahl auf die Internetadresse des Wahlportals sowie auf Beginn und Ende des Wahlzeitraumes (Beginn und Schluss der Möglichkeit zur Stimmabgabe) und den Ort, an dem eine Stimmabgabe unter Verwendung eines durch die Universität bereitgestellten Computers möglich ist,
    2. die Regelungen für die Stimmabgabe,
    3. die zugelassenen Wahlvorschläge,
    4. die Feststellung der Wahlleitung nach §12.
2. Die Wahlbekanntmachung soll mindestens eine Woche vor dem ersten Tag des Wahlzeitraums öffentlich bekannt gemacht werden. Erfolgt die öffentliche Bekanntmachung nach §22 durch Aushang, so darf der Aushang erst nach Ablauf der für die Durchführung der Wahl festgesetzten Zeit enden.

## §14 Stimmzettel

1. Die Stimmzettel (Papier beziehungsweise digital) sind gesondert für die Wahl jedes Organs sowie getrennt für jeden Wahlbereich herzustellen und müssen eine entsprechende Überschrift tragen. Bei Wahlvorschlägen, die mit einem Kennwort versehen sind, ist das Kennwort auf dem Stimmzettel anzugeben. Stimmzettel in Papierform müssen mit dem Motiv des Universitätssiegels versehen werden oder falls nicht zulässig, mit einem, das durch die Wahlleitung festgelegt wird. Im Falle einer digitalen Wahl können der digitale Stimmzettel und der Papierstimmzettel unterschiedlich gestaltet werden.
2. Bei Listenwahl sind auf dem Stimmzettel die Wahlvorschlagslisten in der Reihenfolge ihres Eingangs abzudrucken. Bei gleichzeitigem Eingang entscheidet das durch die Wahlleitung zu ziehende Los. Sofern die Wahlleitung nichts Abweichendes beschließt, gelten als zeitgleich eingegangen die Wahlvorschläge, die, wie folgt, eingegangen sind
    a) montags bis donnerstags jeweils am selben Tag bis einschließlich 15:00 Uhr; danach eingehende Wahlvorschläge werden dem darauffolgenden Tag zugerechnet,
    b) freitags bis einschließlich 12:00 Uhr oder
    c) von Freitag nach 12:00 Uhr bis zum darauffolgenden Montag um 09:00 Uhr. 4 Innerhalb eines Listenwahlvorschlags sind die Namen und Vornamen der Bewerber\*innen entsprechend der Reihenfolge im eingereichten Wahlvorschlag aufzuführen. Der Stimmzettel muss Raum für das Ankreuzen der einzelnen Bewerber\*innen des Listenwahlvorschlags vorsehen.
3. Bei Mehrheitswahl sind alle Bewerber\*innen auf dem Stimmzettel in alphabetischer Reihenfolge und gegebenenfalls mit dem Kennwort als Zusatz aufzuführen. Bei jeder\*jedem Bewerber\*in ist Raum für das Ankreuzen vorzusehen.
4. Auf dem Stimmzettel ist deutlich darauf hinzuweisen, wie viele Bewerber\*innen höchstens anzukreuzen sind. Bei Listenwahl ist zusätzlich darauf hinzuweisen, dass die Stimme für eine Bewerber\*in auch zugunsten der gesamten Liste gezählt wird.

## §15 Stimmabgabe bei Urnenwahl

1. Jede\*jeder Wahlberechtigte hat ihre\*seine Stimme auf dem Stimmzettel durch Ankreuzen oder auf andere Weise an der neben dem Namen jeder\*jeden Bewerber\*in dafür vorgesehenen Stelle persönlich abzugeben. Eine wählende Person, die des Lesens unkundig oder durch körperliche Beeinträchtigung gehindert ist, den Stimmzettel zu kennzeichnen oder in die Wahlurne zu legen, kann sich der Hilfe einer anderen Person bedienen. Auf Wunsch der wählenden Person soll eine der aufsichtführenden Personen Hilfe leisten. Bei Listenwahl hat jede\*jeder Wähler\*in nur eine Stimme. Bei Mehrheitswahl in einem Wahlbereich entspricht die Anzahl der maximal wählbaren Bewerber\*innen der Gesamtsitzzahl des Gremiums; Stimmenhäufung auf eine\*einen Bewerber\*in ist unwirksam.
2. Es ist sicherzustellen, dass die\*der Wähler\*in den Stimmzettel im Wahlraum unbeobachtet kennzeichnen und abgeben kann. Entsprechende Vorkehrungen hat die Wahlleitung in Abstimmung mit den Wahlkoordinator\*innen zu treffen. Für die Aufnahme der Stimmzettel sind Wahlurnen zu verwenden. Vor Beginn der Stimmabgabe sind die leeren Wahlurnen so zu verschließen, dass die gefalteten Stimmzettel nur durch einen Spalt in den Deckel eingeworfen werden können.
3. Solange der Wahlraum zur Stimmabgabe geöffnet ist, müssen mindestens zwei Wahlhelfende (Aufsichtführende) im Wahlraum anwesend sein, die sich in einer Anwesenheitsliste einzutragen haben. (Satz 2 Entfällt) Ein Exemplar dieser Wahlordnung soll zur Einsichtnahme im Wahlraum ausliegen.
4. Vor Ausgabe des Stimmzettels haben die Aufsichtführenden festzustellen, ob die\*der Wahlberechtigte im Wahlverzeichnis eingetragen ist. Die Stimmabgabe ist in einer Ausfertigung oder in einem Auszug des Wahlverzeichnisses zu vermerken. Wenn die Wahlberechtigung durch einen Wahlschein nachgewiesen wird, ist dieser mit dem Vermerk, dass das Wahlrecht ausgeübt ist, zu den Wahlunterlagen zu nehmen. Die\*der Wahlberechtigte muss sich auf Verlangen der Aufsichtführenden durch einen amtlichen oder universitären Lichtbildausweis ausweisen.
5. Wird die Wahlhandlung unterbrochen oder wird das Ergebnis nicht unmittelbar nach Stimmabgabe festgestellt, ist eine Wahlurne in einem verschlossenen Raum zu verwahren. Die\*der Wahlkoordinator\*in stellt im Benehmen mit den Aufsichtführenden sicher, dass die Wahlurne bei einem sich über mehrere Tage erstreckenden Wahlzeitraum jeweils außerhalb der Abstimmungszeit in einem verschlossenen Bereich verwahrt wird. Zu Beginn und bei Wiedereröffnung der Wahl und bei der Entnahme der Stimmzettel zur Auszählung haben sich die\*der Wahlkoordinator\*in und mindestens eine\*ein Aufsichtführende\*r davon zu überzeugen, dass der Verschluss der Wahlurne unversehrt ist.
6. Der Wahlraum muss allen dort Wahlberechtigten zugänglich sein. Die Aufsichtführenden ordnen bei Andrang den Zutritt zum Wahlraum und sorgen im Übrigen dafür, dass während der Wahlhandlung jede unzulässige Wahlbeeinflussung unterbleibt.
7. Im Wahlraum sowie in einem Abstand von 5 m zum Wahlraum oder von 2 m zum Eingang des Wahlraums ist jede Beeinflussung der Wähler\*innen durch Wort, Ton, Schrift oder Bild verboten. Das gilt nicht für Bekanntmachungen der Wahlorgane. Das Anbringen von Wahlwerbung ist zuvor bei der für den Bereich oder das Gebäude zuständigen Stelle anzumelden. Die Universität soll Wahlwerbung ermöglichen. Das Anbringen von Wahlwerbung kann vorab oder nachträglich mit Auflagen versehen oder untersagt werden, sofern die Gefahr besteht, dass
    a) durch Art und Umfang der angemeldeten Werbung bestehende oder mögliche Werbemaßnahmen anderer Hochschulgruppen in einer den Gleichheitsgrundsatz verletzender Weise in nicht nur unerheblichem Umfang beeinträchtigt oder ausgeschlossen werden,
    b) der Wissenschafts- und Vorlesungsbetrieb in nicht nur unerheblicher Weise oder sicherheitsrechtliche Bestimmungen in irgendeiner Weise beeinträchtigt werden.Nach Ablauf der für die Stimmabgabe festgesetzten Tageszeit dürfen nur noch die Wahlberechtigten abstimmen, die sich zu diesem Zeitraum im Wahlraum befinden.

    Der Zutritt zum Wahlraum ist so lange zu sperren, bis die anwesenden Wähler\*innen ihre Stimmen abgegeben haben.

## §15a Stimmabgabe bei digitaler Wahl

1. Die Wahlberechtigten erhalten gemäß §8 ihre Wahlbenachrichtigung. Diese beinhaltet neben den Informationen zur Wahlberechtigung, dem Antrag auf Erklärung der Zugehörigkeit und dem Antrag auf Briefwahl die Informationen zur Authentifizierung, zur Durchführung der Wahl und zur Nutzung des Wahlportals. Das Wahlportal ermöglicht die Stimmabgabe mittels Aufrufes eines digitalen Stimmzettels.
2. Die Stimmabgabe in elektronischer Form hat frei und geheim durch die\*den Wählenden zu erfolgen. Die Authentifizierung der Wahlberechtigten erfolgt durch das Einloggen mit den persönlichen Authentifizierungsmerkmalen (zum Beispiel Matrikelnummer beziehungsweise studentische E-Mail-Adresse und Passwort) am Wahlportal, über das die\*der Wählende per sicherem Link zur Überprüfung der Wahlberechtigung an das digitale Wahlverzeichnis weitergeleitet wird. Der elektronische Stimmzettel ist entsprechend den in der Wahlbenachrichtigung und im Wahlportal enthaltenen Anleitungen digital auszufüllen und abzusenden. Dabei ist durch das verwendete elektronische Wahlsystem sicherzustellen, dass das Stimmrecht nicht mehrfach ausgeübt werden kann. Die Speicherung der abgesandten Stimmen muss anonymisiert und so erfolgen, dass die Reihenfolge des Stimmeingangs nicht nachvollzogen werden kann. Die Wahlberechtigten haben bis zum Absenden der Stimmabgabe die Möglichkeit, ihre Eingabe anzusehen, zu korrigieren oder die Wahl abzubrechen. Ein Absenden der Stimme ist erst auf der Grundlage einer digitalen Bestätigung durch den Wähler zu ermöglichen. Die Übermittlung muss für die\*den Wähler\*in am Bildschirm erkennbar sein. Mit dem Hinweis auf die erfolgreiche Stimmabgabe gilt diese als vollzogen.
3. Bei der Stimmabgabe darf es durch das verwendete digitale Wahlsystem zu keiner Speicherung der Stimme der Wählenden in dem von ihnen hierzu verwendeten Computer kommen. Es muss gewährleistet sein, dass unbemerkte Veränderungen der Stimmeingabe durch Dritte ausgeschlossen sind. Auf dem Bildschirm muss der Stimmzettel nach Absenden der Stimmabgabe unverzüglich ausgeblendet werden. Das verwendete digitale Wahlsystem darf die Möglichkeit für einen Papierausdruck oder eine vergleichbare Perpetuierung der abgegebenen Stimme nach der endgültigen Stimmabgabe nicht zulassen. Die Speicherung der Stimmabgabe in der digitalen Wahlurne muss nach einem nicht nachvollziehbaren Zufallsprinzip erfolgen. Die erfolgreiche Anmeldung im digitalen Wahlsystem nach Authentifizierung am Wahlportal und die IP-Adressen der Wahlberechtigten dürfen nicht protokolliert werden; externe Dienstleister dürfen keine nicht-anonymisierten personenbezogenen Daten der Wahlberechtigten verarbeiten. Bei der Stimmabgabe darf es durch das digitale Wahlsystem zu keiner weitergehenden Verarbeitung kommen als derjenigen, die technisch für die Stimmabgabe erforderlich ist; es ist sicherzustellen, dass zu keiner Zeit eine Zuordnung des Inhalts der Wahlentscheidung zu Wählenden möglich ist.
4. Die Stimmabgabe in digitaler Form ist während der in der Wahlausschreibung festgelegten Dienstzeiten auch an wenigstens einem durch die Wahlleitung festgelegten Ort unter Verwendung eines durch die Universität bereitgestellten Computers möglich.

## §15b Beginn und Ende der digitalen Wahl

Beginn und Beendigung der digitalen Wahl sind nur bei gleichzeitiger Autorisierung durch mindestens zwei berechtigte Personen zulässig und zu protokollieren. Berechtigte sind die Wahlleitung und die\*der Vorsitzende des Wahlausschusses. Für den Fall der Verhinderung können die Wahlleitung eine\*einen Beschäftigte\*n und der Wahlausschuss ein Mitglied aus seiner Mitte benennen.

## §15c Störungen der digitalen Wahl

1. Ist die digitale Stimmabgabe während des Wahlzeitraums aus von der Universität zu vertretenden technischen Gründen den Wahlberechtigten nicht möglich, kann die Wahlleitung im Einvernehmen mit dem Wahlausschuss den Wahlzeitraum verlängern. Die Verlängerung muss allgemein bekannt gegeben werden.
2. Werden während der elektronischen Wahl Störungen bekannt, die ohne Gefahr eines vorzeitigen Bekanntwerdens oder Löschens der bereits abgegebenen Stimmen behoben werden können und ist eine mögliche Stimmenmanipulation ausgeschlossen, kann die Wahlleitung solche Störungen beheben oder beheben lassen und die Wahl fortsetzen; wenn die weitere ordnungsgemäße Durchführung der Wahl nicht gewährleistet ist, ist die Wahl ohne Auszählung der Stimmen abzubrechen. Wird die Wahl fortgesetzt, sind die Störung und deren Dauer im Protokoll zur Wahl zu vermerken. Im Falle des Abbruchs der Wahl entscheidet die Wahlleitung im Einvernehmen mit dem Wahlausschuss über das weitere Verfahren.

## §15d Briefwahl bei digitaler Wahl

1. Wird die Wahl als digitale Wahl durchgeführt, ist die Stimmabgabe auch in der Form der Briefwahl zulässig.
2. Es gelten die Bestimmungen des §16.
3. Mit dem Versand oder der Aushändigung der Briefwahlunterlagen sind die Wahlberechtigten von der elektronischen Stimmabgabe ausgeschlossen.

## §15e Technische Anforderungen

1. Digitale Wahlen dürfen nur dann durchgeführt werden, wenn das verwendete digitale Wahlsystem aktuellen technischen Standards, insbesondere den Sicherheitsanforderungen für Online-Wahlprodukte des Bundesamtes für Sicherheit in der Informationstechnik entspricht. Das digitale Wahlsystem muss die in den nachfolgenden Absätzen aufgeführten technischen Spezifikationen besitzen. Die Erfüllung der technischen Anforderungen ist durch geeignete Unterlagen nachzuweisen. Die Universität kann sich zur Durchführung der digitalen Wahl und zur Feststellung ausreichender Sicherheitsstandards externer Dienstleisters bedienen, welche vertraglich zur Einhaltung der Bestimmungen der Wahlordnung sowie zur Ermöglichung der Kontrolle der Sicherstellung des Datenschutzes durch die Universität zu verpflichten sind.
2. Zur Wahrung des Wahlgeheimnisses müssen digitale Wahlurne und digitales Wahlverzeichnis auf verschiedener Serverhardware geführt werden. Das Wahlverzeichnis wird auf einem universitären Server (einschließlich Server der GWDG) gespeichert.
3. Die Wahlserver müssen vor Angriffen aus dem Netz geschützt sein, insbesondere dürfen nur autorisierte Zugriffe zugelassen werden. Autorisierte Zugriffe sind insbesondere die Überprüfung der Stimmberechtigung, die Speicherung der Stimmabgabe zugelassener Wählender, die Registrierung der Stimmabgabe und die Überprüfung auf mehrfacher Ausübung des Stimmrechtes (Wahldaten). Es ist durch geeignete technische Maßnahmen zu gewährleisten, dass im Falle des Ausfalles oder der Störung eines Servers oder eines Serverbereiches keine Stimmen unwiederbringlich verloren gehen können.
4. Das Übertragungsverfahren der Wahldaten ist so zu gestalten, dass sie vor Ausspäh- oder Entschlüsselungsversuchen geschützt sind. Die Übertragungswege zur Überprüfung der Stimmberechtigung der Wählenden sowie zur Registrierung der Stimmabgabe im digitalen Wahlverzeichnis und die Stimmabgabe in die digitale Wahlurne müssen so getrennt sein, dass zu keiner Zeit eine Zuordnung des Inhalts der Wahlentscheidung zu Wählenden möglich ist.
5. Die Datenübermittlung muss verschlüsselt erfolgen, um eine unbemerkte Veränderung der Wahldaten zu verhindern. Bei der Übertragung und Verarbeitung der Wahldaten ist zu gewährleisten, dass bei der Registrierung der Stimmabgabe im digitalen Wahlverzeichnis kein Zugriff auf den Inhalt der Stimmabgabe möglich ist.
6. Die Wählenden sind über geeignete Sicherungsmaßnahmen zu informieren, mit denen der für die Wahlhandlung genutzte Computer gegen Eingriffe Dritter nach dem aktuellen Stand der Technik geschützt wird; auf kostenfreie Bezugsquellen geeigneter Software ist zu hinzuweisen. Die Kenntnisnahme der Sicherheitshinweise ist vor der Stimmabgabe durch den Wähler verbindlich in digitaler Form zu bestätigen.

## §16 Briefwahl

1. Jede\*jeder Wahlberechtigte kann von der Möglichkeit der Briefwahl Gebrauch machen, wenn die Person das bei der Wahlleitung in der durch die Wahlbekanntmachung festgesetzten Frist persönlich oder schriftlich beantragt. Die Frist für die Beantragung (Ausschlussfrist) darf frühestens mit dem siebenten Tage vor Beginn des Wahlzeitraums enden. Die Frist für die persönliche Beantragung und Entgegennahme der Briefwahl (Ausschlussfrist) darf frühestens mit dem vierten Tage vor Beginn des Wahlzeitraums enden. Weist eine wahlberechtigte Person nach, dass sie ohne eigenes Verschulden die Antragsfrist versäumt hat, kann die persönliche Briefwahlbeantragung und Entgegennahme der Briefwahlunterlagen noch bis 12:00 Uhr am letzten Wahltag erfolgen. Abweichend von Sätzen 3 und 4 endet die Frist für die schriftliche und die persönliche Beantragung der Briefwahl im Falle einer mit einer digitalen Wahl verbundenen Briefwahl zwei Wochen vor Beginn des Wahlzeitraums (Ausschlussfrist). Die Wahlberechtigung ist zu prüfen. Nachdem in das Wahlverzeichnis ein Briefwahlvermerk aufgenommen ist, sind die Briefwahlunterlagen auszuhändigen oder zuzusenden. Briefwahlunterlagen sind
    - die Stimmzettel,
    - der Wahlschein,
    - der jeweilige Stimmzettelumschlag, der das zu wählende Organ erkennen lässt,
    - der Rücksendeumschlag und
    - die Briefwahlerläuterung.

    Einer\*einem anderen als der\*dem Wahlberechtigten persönlich dürfen die Briefwahlunterlagen nur ausgehändigt oder zugesandt werden, wenn eine schriftliche Empfangsvollmacht vorliegt.
2. Die\*der Wähler\*in gibt bei der Briefwahl ihre\*seine Stimme in der Weise ab, dass dieser\*diese für jede Wahl einen Stimmzettel persönlich und unbeobachtet kennzeichnet und in dem dafür vorgesehenen Stimmzettelumschlag verschließt; die Verantwortung hierfür obliegt der\*dem Wählenden. Der mit einer entsprechenden Erklärung vervollständigte und unterschriebene Wahlschein ist zusammen mit den Stimmzettelumschlägen im Rücksendeumschlag (nachfolgend gemeinsam: Wahlbrief) persönlich bei der Wahlleitung abzugeben oder dieser zuzusenden.
3. Die Stimmabgabe ist rechtzeitig erfolgt, wenn der Wahlbrief bei der Wahlleitung bis zum Ablauf der für die Stimmabgabe festgesetzten Tageszeit eingegangen ist. Auf dem Wahlbriefumschlag ist der Tag des Eingangs, bei Eingang am letzten Wahltag auch die Uhrzeit zu vermerken. Verspätet eingehende Wahlbriefumschläge hat die Wahlleitung mit einem Vermerk über den Zeitpunkt des Eingangs ungeöffnet zu den Wahlunterlagen zu nehmen.
4. Die Wahlleitung hat dafür Sorge zu tragen, dass in Gegenwart von mindestens zwei Aufsichtführenden während des Wahlzeitraums oder unmittelbar im Anschluss daran die ordnungsgemäße Briefwahl geprüft und im Wahlverzeichnis vermerkt wird und dass die Stimmzettelumschläge ungeöffnet in eine allgemein verwendete Wahlurne gebracht werden. Im Falle einer mit einer digitalen Wahl verbundenen Briefwahl finden die Prüfung der ordnungsgemäßen Briefwahl, der Vermerk im Wahlverzeichnis und die Auszählung der Briefwahl unmittelbar nach dem Ende des Wahlzeitraums statt.
5. Die Stimmzettel sind nicht in die Wahlurne zu bringen und eine Stimme gilt als nicht abgegeben, wenn
    1. der Wahlbrief nicht rechtzeitig bei der Wahlleitung eingegangen ist,
    2. die Wähler\*in nicht im Wahlverzeichnis als briefwahlberechtigt vermerkt ist,
    3. der Wahlbrief keinen gültigen und um die Erklärung nach Absatz 2 vervollständigten Wahlschein enthält,
    4. der Wahlbrief mehrere Stimmzettelumschläge, aber nicht die gleiche Anzahl gültiger Wahlscheine enthält,
    5. die Briefwähler\*in gegen die Briefwahlregelung verstoßen hat und deswegen nicht sichergestellt ist, dass ihr\*sein Stimmzettelumschlag ungeöffnet in die Wahlurne gebracht werden kann, insbesondere wenn der Stimmzettel in einem nichtamtlichen oder unverschlossenen Stimmzettelumschlag oder offen im Wahlbrief liegt,
    6. der Wahlbrief oder der Stimmzettelumschlag neben dem Stimmzettel einen fühlbaren Gegenstand enthält.
6. Die Universität hat die Briefwähler\*innen von Portokosten des innerdeutschen Postverkehrs auf Antrag freizustellen, solange ein Mitglied des Universitätspräsidiums die Wahlleitung innehat; im Übrigen hat die Studierendenschaft die Briefwähler\*innen auf Antrag von Portokosten des innerdeutschen Postverkehrs freizustellen.
7. Verlorene Briefwahlunterlagen werden nicht ersetzt. Versichert eine wahlberechtigte Person glaubhaft, dass ihr die Briefwahlunterlagen, ohne eigenes Verschulden, nicht zugegangen sind, können ihr noch bis 12:00 Uhr am letzten Wahltag, neue Briefwahlunterlagen ausgehändigt werden. Die Wahlleitung stellt die Ungültigkeit der nicht zugegangenen Briefwahlunterlagen fest und ergänzt das Wahlverzeichnis um einen Vermerk.

## §17 Auszählung

1. Der Wahlausschuss oder die Wahlkoordinator\*innen haben nach Abschluss der Stimmabgabe unverzüglich, spätestens an dem auf den letzten Wahltag folgenden Werktag, die in ihrem Zuständigkeitsbereich abgegebenen Stimmen unter Hinzuziehung von Wahlhelfenden zu zählen; bei nicht unmittelbar folgender Auszählung gilt §15 Abs. 5 entsprechend. Zunächst ist die Zahl der in den Urnen enthaltenen Stimmzettel mit der Zahl der Stimmabgaben zu vergleichen, die in einer Ausfertigung oder in einem Auszug des Wahlverzeichnisses vermerkt sind. Ist die Zahl der Stimmzettel höher als die der vermerkten Stimmabgaben, so hat der Wahlausschuss bei der Feststellung des Wahlergebnisses festzustellen, ob die Zahl der unzulässig abgegebenen Stimmzettel Einfluss auf die Sitzverteilung gehabt haben könnte.
2. Die auf jeden Wahlvorschlag entfallenden gültigen Stimmen werden zusammengezählt. Ungültig sind Stimmen, wenn der Stimmzettel
    1. nicht als amtlich erkennbar ist,
    2. keinen Stimmabgabevermerk enthält,
    3. den Willen der\*des Wählenden nicht zweifelsfrei ergibt,
    4. neben dem Stimmabgabevermerk weitere Anmerkungen oder Kennzeichnen, zum Beispiel einen Vorbehalt oder einen Kommentar enthält,
    5. bei Mehrheitswahl Stimmenhäufung auf eine Bewerber\*innen oder mehr als die höchstens zulässige Zahl an Stimmabgabevermerken enthält.
3. Die Wahlkoordinator\*innen entscheiden über Stimmzettel, die Anlass zu Bedenken geben und haben der Wahlleitung anschließend mitzuteilen, ob und wie der Stimmzettel vorläufig gezählt worden ist. Der Wahlausschuss entscheidet bei als solchen gekennzeichneten Zweifelsfällen abschließend, ob und wie der Stimmzettel zu zählen ist, und bestätigt oder berichtigt entsprechend dieser Entscheidung das Zählergebnis. Diese Stimmzettel sind mit fortlaufender Nummer zu versehen und von den übrigen Stimmzetteln gesondert bei den Wahlunterlagen aufzubewahren.
4. Nach Abschluss der Auszählung sind die Niederschriften über die Wahlhandlung und die Auszählung sowie die Ausfertigungen oder Auszüge aus dem Wahlverzeichnis, die Wahlscheine und die Stimmzettel unverzüglich der Wahlleitung zur Weiterleitung an den Wahlausschuss zu übergeben.
5. Im Falle der digitalen Wahl mit Briefwahlmöglichkeit ist für die Administration der Wahlserver und insbesondere für die Auszählung und Archivierung der digitalen Wahl die Autorisierung durch mindestens zwei Berechtigte der Wahlleitung oder des Wahlausschusses notwendig. Die Wahlleitung veranlasst unverzüglich nach Beendigung der digitalen Wahl universitätsöffentlich die computerbasierte Auszählung der digital abgegebenen Stimmen und stellt das Ergebnis durch einen Ausdruck der Auszählungsergebnisse fest, der von zwei Mitgliedern des Wahlausschusses abgezeichnet wird. Alle Datensätze der digitalen Wahl sind in geeigneter Weise zu speichern.
6. Bei digitalen Wahlen sind technische Möglichkeiten zur Verfügung zu stellen, die den Auszählungsprozess für jede Wählerin oder jeden Wähler jederzeit reproduzierbar machen.

## §18 Feststellung des Wahlergebnisses

1. Der Wahlausschuss stellt auf Grund der Zählergebnisse, die er überprüfen kann, als Wahlergebnis gesondert für jeden Wahlbereich fest:
    1. die Zahl der Wahlberechtigten,
    2. die Zahl der Wählerinnen oder Wähler,
    3. die Zahl der ungültigen Stimmzettel,
    4. die Zahl der gültigen Stimmen,
    5. die Zahl der Stimmen, die auf die einzelnen Wahlvorschläge insgesamt und auf die einzelnen Bewerberinnen und Bewerber entfallen sind,
    6. die gewählten Vertreter\*innen und Ersatzleute,
    7. das Zustandekommen oder Nichtzustandekommen der Wahl einschließlich einer gegebenenfalls erforderlichen Nachwahl. Im Falle einer digitalen Wahl gilt Satz 1 entsprechend.
2. Die Sitzzuteilung und Vertretungsregelung der zu wählenden Organe ergeben sich aus §7 OrgS; nachfolgende Regelungen gelten nur, sofern dort nichts anderes bestimmt ist. Die danach einem Listenwahlvorschlag zustehenden Sitze erhalten die Bewerber\*innen dieses Wahlvorschlags, die die höchste Stimmenzahl erreicht haben, nach der Reihenfolge ihrer Stimmenzahl. (Satz 3 entfällt) Bewerber\*innen eines Listenwahlvorschlags, die keinen Sitz erhalten, sind nach der Reihenfolge ihrer Stimmenzahl Ersatzleute und rücken für die gewählten Bewerber\*innen nach, wenn diese vorzeitig aus dem betreffenden Organ ausscheiden. Bei gleicher Stimmenzahl und wenn auf mehrere Bewerber\*innen keine Stimme entfallen ist, entscheidet die Reihenfolge der Bewerber\*innen innerhalb eines Listenwahlvorschlags. Wenn eine Liste ausgeschöpft ist, rückt die erste Ersatzperson des Wahlvorschlags nach, auf den nach Satz 1 ein weiterer Sitz entfallen würde. Stimmen, die auf Personen entfallen sind, deren Mitgliedschaft für einen Wahlbereich nach Feststellung des vorläufigen Wahlverzeichnisses endet oder ruht und die hierdurch das Wahlrecht verloren haben, zählen nur zugunsten der Liste.
3. Bei Mehrheitswahl werden die Gesamtzahl der Sitze des Gremiums auf die Bewerber\*innen aller Wahl(teil)bereiche nach der Reihenfolge der auf sie entfallenen Stimmen mit der höchsten Stimmenzahl beginnend verteilt. In gleicher Weise werden die Ersatzleute bestimmt. Absatz 2 Satz 4 gilt entsprechend.
4. Wenn in den Fällen der Absätze 2 bis 3 gleiche Höchstzahlen oder Stimmenzahlen vorliegen, entscheidet, wenn nichts anderes bestimmt ist, das Los, das die\*der Vorsitzende des Wahlausschusses zieht; hierfür kann er\*sie ein Mitglied des Wahlausschusses oder die Wahlleitung mit der Ziehung des Loses beauftragen.
5. In die Feststellung des Wahlergebnisses sind auch die Personen aufzunehmen, die als Gewählte gelten, weil zum Zeitpunkt der Wahl einem Wahlbereich nicht mehr Wahlberechtigte angehören, als Vertreter\*innen zu entsenden sind; in diesem Fall ist die Einreichung eines Wahlvorschlags entbehrlich.
6. Die Wahlen sind für das gesamte Organ zustande gekommen, wenn mehr als die Hälfte der stimmberechtigten Mitglieder gewählt worden ist; (Rest entfällt) Soweit eine Wahl nicht zustande gekommen ist, haben die bisherigen Mitglieder ihre Geschäfte bis zum Beginn der Amtszeit der neu zu wählenden Mitglieder fortzuführen.
7. Der Wahlausschuss hat das Wahlergebnis der Wahl zu den Organen festzustellen, im Falle der digitalen Wahl als Gesamtergebnis der digitalen Wahl und der Briefwahl. Die Wahlleitung macht das Wahlergebnis unverzüglich öffentlich bekannt; dabei ist auf die Möglichkeit hinzuweisen, nach §23 Abs. 1 Einspruch einzulegen, unter Angabe der Einspruchsfrist und der Stelle, bei der Einspruch einzulegen ist. Die gewählten Mitglieder und die Ersatzleute im Falle ihres Nachrückens sind von der Wahlleitung zu benachrichtigen.

## §19 Besondere Sitzverteilung bei Rücktritt

Stehen nach Feststellung des Wahlergebnisses auf Grund des Rücktritts von Bewerber\*innen einem Wahlvorschlag mehr Sitze zu, als Bewerber\*innen benannt sind, so werden durch die Wahlleitung die freien Sitze entsprechend §18 Abs. 2 Satz 6 auf die übrigen Wahlvorschläge verteilt.

## §20 Nach-, Ergänzungs- und Neuwahl

1. Eine Nachwahl findet statt, wenn
    1. in einzelnen Wahlbereichen eine Wahl nicht durchgeführt worden ist, weil die Zahl der Wahlberechtigten zunächst die Zahl der zu besetzenden Sitze nicht überstieg, wenn jedoch bis zur Hälfte der Amtszeit die Zahl der Wahlberechtigten über die Gesamtsitzzahl des Gremiums gestiegen ist und die Durchführung der Nachwahl von einer wahlberechtigten Person beantragt wird;
    2. in einzelnen Wahlbereichen die Wahl nicht durchgeführt worden ist, weil das Wahlverfahren auf Grund eines Beschlusses des Wahlausschusses wegen eines Verstoßes gegen Wahlrechtsvorschriften unterbrochen ist;
    3. Verstöße gegen Wahlrechtsvorschriften sich auf das Wahlergebnis ausgewirkt haben oder ausgewirkt haben können;
    4. nach der Feststellung des Wahlergebnisses die Wahl nicht zustande gekommen ist oder wenn aus anderen Gründen nicht alle Sitze eines Organs besetzt werden können, es sei denn, dass bereits eine Nachwahl oder eine Wiederholung der Wahlausschreibung erfolgt ist und eine weitere Nachwahl kein anderes Ergebnis verspricht.

    Wenn eine Nachwahl notwendig ist, stellt dies der Wahlausschuss fest; zugleich bestimmt er, auf welche Wahlbereiche, Wahlteilbereiche oder Personen innerhalb einer Liste sich die Nachwahl erstreckt. Der Wahlausschuss kann die Wahlprüfung auf den in einem Wahleinspruch substantiiert dargelegten Sachverhalt sowie das in dem Wahleinspruch genannte Organ beschränken. Dieser Beschluss ist in der erneuten Wahlausschreibung öffentlich bekannt zu machen. Die Nachwahl kann vor Abschluss der verbundenen Wahl vorbereitet werden.
2. Eine Ergänzungswahl findet statt, wenn während der Amtszeit eines Organs eines seiner Mitglieder ausscheidet, keine Ersatzleute mehr nachrücken können und der Sitz auch nicht im Verfahren nach §19 besetzt werden kann. Eine entsprechende Feststellung hat das betreffende Organ zu treffen. Auf eine Ergänzungswahl kann verzichtet werden, wenn die Zahl der Mitglieder des Organs mehr als die Hälfte der vorgeschriebenen Zahl beträgt oder wenn nur noch eine Sitzung des Organs in der laufenden Wahlperiode zu erwarten ist. Der Verzicht auf die Ergänzungswahl muss von den betroffenen Mitgliedern des Organs beschlossen werden.
3. Für Nach- und Ergänzungswahlen gelten die für die verbundenen Wahlen von Organen getroffenen Regelungen. Der Wahlausschuss kann im Einzelfall durch Beschluss, der öffentlich bekannt zu machen ist, davon abweichende Bestimmungen über Fristen und andere Bestimmungen zur Wahldurchführung sowie über Bekanntmachungen treffen, soweit gewährleistet ist, dass die Betroffenen ausreichend Gelegenheit erhalten, von der Wahlausschreibung und Wahlbekanntmachung Kenntnis zu nehmen sowie Einsprüche und Wahlvorschläge einzureichen. Die Abstimmung kann auch in einer Wahlversammlung oder als Briefwahl erfolgen, sofern sich die Nachwahl nicht nur auf einen Teilbereich erstreckt. Die Nach- und die Ergänzungswahlen erstrecken sich nur auf die nichtbesetzten Sitze in diesem Organ. Der Wahlausschuss soll festlegen, dass keine neuen Wahlvorschläge eingereicht werden können, insbesondere falls eine Nachwahl ausschließlich in Wahlteilbereichen oder zur Wahl zwischen einzelnen Personen einer Liste durchzuführen ist; in diesem Fall kann die Wahlleitung die Wahlausschreibung mit der Wahlbekanntmachung verbinden und die gemessen am Umfang der Nachwahl erforderlichen Stimmzettel festlegen.
4. Eine Neuwahl findet statt, wenn ein Organ aufgelöst ist; im Übrigen ist Absatz 3 entsprechend anzuwenden. Sie soll innerhalb von acht Wochen und muss innerhalb der Vorlesungszeit stattfinden. Der Wahlausschuss kann die für die verbundene Wahl festgesetzten Fristen zu diesem Zwecke ändern. Ein Verzicht auf die Neuwahl ist nicht möglich. Findet die Neuwahl später als 6 Monate nach Beginn der regelmäßigen Amtszeit der Mitglieder des aufgelösten Organs statt, so entfällt die Wahl für dieses Organ bei der nächsten verbundenen Wahl; in diesem Fall ist in der Wahlausschreibung und der Wahlbekanntmachung zur Neuwahl darauf hinzuweisen, dass abweichend von der regelmäßigen Amtszeit die Mitglieder im neu gewählten Organ bis zur übernächsten verbundenen Wahl amtieren werden. Andernfalls amtieren die Mitglieder des neu gewählten Organs regulär bis zur nächsten verbundenen Wahl.
5. Ein Fachschaftsparlament kann bei Einrichtung einer neuen Fachgruppe die Erstwahl einer Fachgruppensprecher\*in auch außerhalb der verbundenen Wahlen vorsehen, sofern die neu geschaffene Fachgruppe Studienfächer vertritt, die bis dahin nicht durch eine Fachgruppe vertreten wurden. Absatz 4 gilt sinngemäß entsprechend. Abweichend von Satz 1 kann das Fachschaftsparlament bei Neueinrichtung einer Fachgruppe selbst mit Zweidrittelmehrheit seiner Mitglieder eine\*n Fachgruppensprecher\*in wählen; in diesem Fall finden Sätze 1 und 2 keine Anwendung.

## §21 Niederschriften

1. Niederschriften sind zu fertigen über Sitzungen des Wahlausschusses und über den Gang der Wahlhandlung.
2. Die Niederschrift über die Sitzung des Wahlausschusses (Ergebnisprotokoll) muss Ort und Zeit der Sitzung, die Namen der Sitzungsteilnehmer\*innen, die Tagesordnung, den wesentlichen Verlauf der Sitzung und alle Beschlüsse enthalten. Die Niederschriften sind von der\*dem Vorsitzenden zu unterzeichnen. Ist eine\*ein Vorsitzende\*r nicht anwesend, so unterzeichnet an ihrer\*seiner Stelle die Stellvertretung.
3. Die Niederschrift über die Durchführung der Wahl muss Ort und Zeit der Wahlhandlung, die Namen der Aufsichtführenden mit der Zeit ihrer Anwesenheit sowie eine kurze Schilderung des Hergangs im Falle besonderer Vorkommnisse enthalten. Die Niederschrift ist von der\*dem Wahlkoordinator\*in zu unterzeichnen. Ist die\*der Wahlkoordinator\*in nicht anwesend, so unterzeichnen an ihrer oder seiner Stelle die Stellvertretung oder zwei Aufsichtsführende.
4. Die Stimmzettel, Wahlscheine und sonstigen Wahlunterlagen sind nach Feststellung des Wahlergebnisses zu bündeln und der Niederschrift über die Durchführung der Wahlhandlung und Auszählung beizufügen.
5. Die Niederschriften nebst Anlagen hat die Wahlleitung aufzubewahren. Die Wahlunterlagen dürfen erst nach Ablauf der Anfechtungsfrist vernichtet werden.

## §22 Fristen und öffentliche Bekanntmachungen

1. Fristen laufen nicht ab an Tagen, die für alle von der Wahl betroffenen Hochschulbereiche vorlesungsfrei sind. Ausschlussfristen enden um 15:00 Uhr, an Freitagen um 12:00 Uhr.
2. Der Wahlausschuss beschließt die Form der öffentlichen Bekanntmachungen der Wahlleitung. Der Beschluss kann auf Bestimmungen des Satzungsrechts der Universität Bezug nehmen und ist öffentlich bekannt zu machen.
3. Falls die öffentlichen Bekanntmachungen der Wahlleitung durch Aushang erfolgen sollen, sind die Aushangstellen genau zu bezeichnen. Für die Universität ist mindestens eine zentrale Aushangstelle vorzusehen. Bekanntmachungen, die lediglich Teilbereiche der Universität betreffen, müssen nur an der zentralen Aushangstelle sowie in den betroffenen Universitätsbereichen ausgehängt werden. Neben der zentralen Aushangstelle können zur besseren Information weitere Aushangstellen bestimmt werden.
4. Bei Aushang gilt die öffentliche Bekanntmachung mit Ablauf des Tages als bewirkt, an dem der Aushang an der zentralen Aushangstelle erfolgt ist. Beginnend mit diesem Zeitpunkt dauert ein vorgeschriebener Aushang eine Woche. Wenn in der Bekanntmachung Einspruchs-, Vorschlags- oder andere Fristen enthalten sind, darf der Aushang nicht vor Ablauf dieser Fristen beendet werden. Kurze Unterbrechungen des Aushangs, die nicht durch Wahlorgane veranlasst werden, sind bei der Berechnung des Aushangzeitraums nicht zu berücksichtigen.
5. Auf jeder an einer zentralen Aushangstelle ausgehängten Ausfertigung der Bekanntmachung soll die Aushangstelle sowie der Beginn und das Ende des Aushangzeitraums vermerkt werden. Diese Ausfertigung der Bekanntmachung ist mit den anderen Wahlunterlagen aufzubewahren.
6. Soweit ein Bekanntmachungstext außerhalb der zentralen Aushangstelle ausgehängt wird, ist es ohne Einfluss auf die Wirksamkeit der öffentlichen Bekanntmachung, wenn dieser Aushang fehlerhaft ist oder unterlassen wird.

## §23 Wahlprüfung

1. Die Wahl kann durch Einspruch, der die Gründe angeben muss, binnen einer Woche nach Bekanntgabe des Wahlergebnisses angefochten werden (Ausschlussfrist). Der Einspruch muss schriftlich, in Textform oder zur Niederschrift gegenüber der Wahlleitung eingelegt werden. Der Einspruch kann nicht mit der Unrichtigkeit des Wahlverzeichnisses begründet werden. Im Falle einer digitalen Wahl können Wahlberechtigte einen Wahleinspruch nicht mit ihren Systemeinstellungen (z. B. „hochsicher“), ihren defekten, veralteten oder seltenen Computern oder Systemen, die einen Zugriff auf das digitale Wahlsystem verhindern oder nicht ermöglichen, oder ihrer Internetverbindung, z. B. Netzstärke oder Unterbrechungen, begründen. Der Wahleinspruch ist begründet, wenn Wahlrechtsbestimmungen verletzt worden sind, die Verletzung unverzüglich gegenüber einer aufsichtführenden Person im Wahllokal oder gegenüber der Wahlleitung angezeigt worden ist und diese Verletzungen zu einer fehlerhaften Feststellung der Gewählten und der jeweils ersten Ersatzperson geführt haben oder geführt haben können. Der Wahleinspruch der oder der Wahlleitung ist unmittelbar an den Wahlausschuss zu richten. Der Wahleinspruch anderer Personen muss damit begründet werden, dass die Wahl Organe betrifft, zu deren Wahl die Person wahlberechtigt ist; ein solcher Wahleinspruch ist bei der Wahlleitung einzureichen und mit deren Stellungnahme unverzüglich dem Wahlausschuss zur Entscheidung vorzulegen.
2. Der Wahlausschuss kann von Amts wegen jederzeit eine Wahlprüfung einleiten.
3. Erwägt der Wahlausschuss, einem Wahleinspruch stattzugeben oder ist er von Amts wegen in die Wahlprüfung eingetreten, hat er diejenigen anzuhören und am Verfahren zu beteiligen, die möglicherweise als Gewählte oder Ersatzleute von einer Entscheidung betroffen sein können; im Falle der Listenwahl tritt an die Stelle der Betroffenen die\*der Listenverantwortliche. Führt der Wahleinspruch zu einer Änderung des Wahlergebnisses, stellt der Wahlausschuss das Wahlergebnis entsprechend der berichtigten Auszählung neu fest. Kann ein richtiges Wahlergebnis nicht mit Sicherheit ermittelt werden, ist entsprechend §20 Abs. 1 Satz 1 Nr. 3 zu verfahren. Der Wahlausschuss kann beschließen, von einer Nachwahl abzusehen, sofern
    a) eine Verletzung von Wahlrechtsbestimmungen in einem minder schweren Fall vorliegt,
    b) sich diese Verletzung nur auf die Sitzverteilung zwischen Personen innerhalb einer Liste ausgewirkt haben kann,
    c) alle betroffenen Personen in Textform zustimmen und
    d) die Verletzung von Wahlrechtsbestimmungen nicht von Kandidierenden oder sonstigen Personen der betroffenen Liste zu vertreten ist.
4. Die Entscheidung ist von der Wahlleitung der wahlberechtigten Person, die den Einspruch erhoben hat, sowie allen, die als Gewählte oder Ersatzleute von der Entscheidung betroffen sind, bekanntzugeben; im Falle der Listenwahl tritt an die Stelle der Betroffenen die\*der Listenverantwortliche.

## §24 Beginn, Dauer und Ende der Amtszeit; Nachrücken

(entfällt)

## §25 Stellvertretung

(entfällt)

## §26 Besondere Bestimmungen

1. Wenn im Falle einer „erheblichen Beeinträchtigung des Universitätsbetriebs“ weitere Maßnahmen zur Verhinderung der Ursache der Beeinträchtigung dies erfordern und eine Beschlussfassung des Studierendenparlaments zur Wahlordnung nicht rechtzeitig möglich ist, kann die Wahlleitung im Einvernehmen mit dem Wahlausschuss abweichende Bestimmungen für die Durchführung der Wahlen treffen, insbesondere zu öffentlichen Bekanntmachungen, zur Einreichung von Wahlvorschlägen, zum Wahlverfahren (Präsenz-, Digital- und/oder Briefwahl), zu Formvorschriften sowie zu Fristen und anderen Zeitbestimmungen festlegen. Abweichende Bestimmungen nach Satz 1 sind unverzüglich hochschulöffentlich bekannt zu machen.
2. Zur Festlegung der Zuständigkeiten bei der Durchführung der Wahl, kann der AStA einen Vertrag mit dem Universitätspräsidium mit einer Vertragsdauer von längstens zwei Jahren schließen, welche durch das Studierendenparlament mit absoluter Mehrheit zu bestätigen ist. Sieht dieser Vertrag vor, dass die Wahlen als digitale Wahlen durchgeführt werden, ist dieser Vertrag nur wirksam, sofern die Universität alle verbundenen Kosten übernimmt oder das Studierendenparlament dieser Vereinbarung mit einer Mehrheit von Zweidritteln zustimmt. Bedingt der Vertrag Änderungen dieser oder einer anderen Ordnung der Studierendenschaft, gilt der Vertrag nur, sofern das zuständige Organ diese Änderung mit der erforderlichen Mehrheit beschließt.

## §27 Inkrafttreten; Übergangsbestimmungen

1. Diese Ordnung tritt am Tage nach ihrer Veröffentlichung in den Amtlichen Mitteilungen I der Georg-August-Universität Göttingen in Kraft.
2. Die Wahlen im Wintersemester 2020/2021, Sommersemester 2021, Wintersemester 2021/2022 und Sommersemester 2022 werden als digitale Wahlen mit Briefwahlmöglichkeit durchgeführt; abweichend hiervon können die Wahlleitung und der Wahlausschuss im Falle einer Nach-, Ergänzungs- oder Neuwahl oder falls digitale Wahlen nach den Bestimmungen dieser Ordnung nicht durch durchführbar sind, im Einvernehmen beschließen, dass eine Urnenwahl mit Briefwahlmöglichkeit durchgeführt wird. Die Wahlleitung obliegt dem Mitglied des Präsidiums für Finanzen und Personal (im Folgenden: Präsidiumsmitglied für Finanzen und Personal); die Bestimmungen der Wahlordnung für die Wahlen zu den Kollegialorganen an der Georg-August-Universität Göttingen zur Delegation der Wahlleitung gelten entsprechend und danach bereits zuvor erfolgte Delegationen gelten als nach dieser Ordnung vorgenommen. Sätze 1 und 2 gelten nur, sofern und solange das Präsidiumsmitglied für Finanzen und Personal die Wahlleitung wahrnimmt.
3. Für die Wahlen im Wintersemester 2020/2021 und Sommersemester 2021 gilt abweichend von §2 Abs. 2 Folgendes: Der Wahlausschuss hat vier Mitglieder. Den Wahlausschuss bilden die studentischen Mitglieder und stellvertretenden Mitglieder, die gemäß §2 Abs. 2 der Wahlordnung für die Wahlen zu den Kollegialorganen an der Georg-August-Universität Göttingen dem Wahlausschuss für die Wahlen zu den Kollegialorganen an der Georg-August-Universität Göttingen angehören. Bereits vor Inkrafttreten dieser Ordnung erfolgte Benennungen und Einladungen zu Sitzungen gelten als nach dieser Ordnung vorgenommen. Die erste Sitzung des Wahlausschusses findet am 03.11.2020 statt.
4. Die Bestimmungen dieser Ordnung zu digitalen Wahlen, insbesondere §§1 Abs. 3 Satz 2 und 15a bis 15e, treten mit Ablauf des Sommersemesters 2022 außer Kraft.

































