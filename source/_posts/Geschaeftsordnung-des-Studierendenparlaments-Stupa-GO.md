---
title: Geschäftsordnung des Studierendenparlaments der Georg-August-Universität Göttingen
date: 2021-04-09
tags:
  - Geschäftsordnung
  - GO
  - StuPa-GO
  - StuPa
  - Studierendenparlament
fassung_der_bekanntmachung:
  date: 2020-01-16
  ort: Amtliche Mitteilungen I 3/2020, S. 33
zuletzt_geaendert:
  gremium: Studierendenparlament
  date: 2021-03-17
  ort: Amtliche Mitteilungen I 18/2021 S. 281 f.
---

Geschäftsordnung des Studierendenparlaments der Georg-August-Universität Göttingen (StuPa-GO) in der Fassung der Bekanntmachung vom 16.01.2020 (Amtliche Mitteilungen I 3/2020, S. 33), zuletzt geändert durch Beschluss des Studierendenparlaments vom 17.03.2021 (Amtliche Mitteilungen I 18/2021, S. 281 f.)

# Geschäftsordnung des Studierendenparlaments der Georg-August-Universität Göttingen

## Abschnitt I: Präsidium

### §1 Sitzungsleitung

1. Dem Präsidium des Studierendenparlaments gehören neben der Präsidentin oder dem Präsidenten zwei Stellvertreterinnen und Stellvertreter an (§13 I 1 OrgS). Das Präsidium leitet die Sitzung (§8 IV 1 OrgS). Des Weiteren vertreten die Mitglieder des Präsidiums das Studierendenparlament nach außen und gegebenenfalls gegenüber anderen Organen der Selbstverwaltung der Universität.
2. Dem Präsidium obliegt, unbeschadet etwaiger Anträge zur Geschäftsordnung, die Auslegung der Organisationssatzung, dieser Geschäftsordnung und aller weiteren Ordnungen der Studierendenschaft für alle die Verhandlung betreffenden Fragen. Dabei entscheidet das Präsidium durch Abstimmung; bei Stimmengleichheit ist die Stimme der Präsidentin oder des Präsidenten ausschlaggebend. Dies gilt für die erste Stellvertreterin oder den ersten Stellvertreter entsprechend (§8 IV 2 – 4 OrgS).
3. Mitglieder des Präsidiums dürfen sich nur in Angelegenheiten der Geschäftsordnung an der Diskussion beteiligen. Um sich in anderen Angelegenheiten zur Sache äußern zu können, muss das jeweilige Mitglied des Präsidiums sich vorübergehend vertreten lassen. Hat das Präsidiumsmitglied sich einmal zu einem Punkt der Tagesordnung geäußert, kann es bis zum Ende der Beratung das Amt des Präsidiumsmitgliedes nicht mehr übernehmen.
4. In Fällen, in denen eine Entscheidung des Studierendenparlaments im Umlaufverfahren vorgesehen ist, trifft die Präsidentin oder der Präsident die hierfür erforderlichen Vorbereitungen.
5. Das Ergebnis der Beschlussfassung im Umlaufverfahren teilt die Präsidentin oder der Präsident dem Studierendenparlament in geeigneter Weise mit.

### §1a Aufgabenverteilung

Die Aufgabenverteilung innerhalb des Präsidiums treffen die Mitglieder des Präsidiums im Konsens. Falls Konsens nicht erreicht werden kann, werden Entscheidungen nach Maßgabe des §1 Abs. 2 getroffen, dabei sollen die Aufgaben zu gleichen Teilen aufgeteilt werden.

### §2 Konstruktives Misstrauensvotum

Das Studierendenparlament kann einem Präsidiumsmitglied das Misstrauen dadurch aussprechen, dass es mit der Mehrheit seiner Mitglieder eine Nachfolgerin oder einen Nachfolger wählt; hierfür gelten die Bestimmungen des §11 III OrgS. Bei einem derartigen Misstrauensantrag leitet ein anderes Präsidiumsmitglied die Verhandlung. Im Übrigen gelten die Bestimmungen der §§8 III, 13 II OrgS.

### §3 Einladung

1. Die Einladung zur Sitzung des Studierendenparlamentes ist zusammen mit den erforderlichen Anlagen und dem Protokoll der letzten Sitzung mindestens sieben Tage vor der Sitzung vom Präsidium wenigstens per Textform an die Mitglieder des Studierendenparlamentes, die Mitglieder des AStAs, das Sportreferat, die Sprecherin oder den Sprecher der FSRV, den ASR und die Fachschaftsräte zu verschicken.
2. Die Einladung enthält die Ankündigung von Personalwahlen und die vorläufige Tagesordnung.
3. Im Falle von Haushalts- und Nachtragshaushaltsberatungen soll zusammen mit der Einladung ein entsprechender Entwurf als Anlage zur Verfügung gestellt werden.
4. Während des Semesters tritt das Studierendenparlament mindestens alle vier Wochen oder auf entsprechenden Antrag nach §13 IV Buchstaben a) bis e) OrgS schnellstmöglich, spätestens innerhalb von 14 Tagen, zusammen.

### §4 Sitzungseröffnung, Beschlussfähigkeit, ordnungsgemäße Ladung, Feststellung der Tagesordnung

1. Zu Beginn jeder Sitzung stellt das Präsidium die Anwesenheit sowie die Beschlussfähigkeit fest. Dann werden die beiden Schriftführerinnen oder Schriftführer bestellt. Eine Wiederbestellung ist zulässig.
2. Auf Antrag zur Geschäftsordnung können die Schriftführerinnen oder Schriftführer auch während einer laufenden Sitzung ausgetauscht werden.
3. Das Studierendenparlament ist beschlussfähig, wenn die Sitzung ordnungsgemäß einberufen wurde und die Mehrheit der stimmberechtigten Mitglieder anwesend ist. Die Sitzungsleitung stellt die Beschlussfähigkeit zu Beginn der Sitzung fest (§6 I OrgS).
4. Das Studierendenparlament gilt sodann, auch wenn sich die Zahl der anwesenden stimmberechtigten Mitglieder im Laufe der Sitzung verringert, als beschlussfähig, solange nicht ein Mitglied Beschlussunfähigkeit geltend macht; dieses Mitglied zählt bei der Feststellung, ob das Gremium noch beschlussfähig ist, zu den anwesenden Mitgliedern. Stellt die Sitzungsleitung die Beschlussunfähigkeit fest, so beruft sie schnellstmöglich mindestens zur Behandlung der nicht erledigten Tagesordnungspunkte eine weitere Sitzung ein (§6 I OrgS).
5. Nach Feststellung der Anwesenheit, Beschlussfähigkeit und Bestellung der beiden Schriftführerinnen oder Schriftführer, beschließt das Studierendenparlament unter Berücksichtigung der gestellten Änderungs- und Ergänzungsanträge über die endgültige Tagesordnung.

### §5 Mitglieder und Stellvertreter\_innen des Studierendenparlaments

1. Mitglieder des Studierendenparlaments sind alle direkt gewählten Mitglieder des Studierendenparlaments entsprechenden der amtlichen Wahlbekanntmachung (vgl. §7 III OrgS). Jedes Mitglied ist im Falle seiner oder ihrer Verhinderung verpflichtet, für ihre oder seine Stellvertretung zu sorgen und dies dem Präsidium anzuzeigen. Bei der Bestimmung der Vertretung ist die Reihenfolge der Stellvertreter\_innen einzuhalten.
2. Stellvertreter\_innen sind die Kandidatinnen und Kandidaten eines Listenwahlvorschlags, die keinen Sitz, aber mindestens eine Stimme erhalten haben in der Reihenfolge der amtlichen Wahlbekanntmachung (§7 IV 1 OrgS).
3. Beratende Mitglieder des Studierendenparlaments sind die Fachschaftssprecherinnen und Fachschaftssprecher, die Sprecherin oder der Sprecher der FSRV und die Sprecherin oder der Sprecher des ASR (§12 VI OrgS).
4. Gäste aus der Studierendenschaft dürfen an der Sitzung teilnehmen. Personen, die die Behandlung eines Gegenstandes erleichtern können und nicht der Studierendenschaft angehören, können zu einem Tagesordnungspunkt als Gäste geladen werden, sofern deren Teilnahme erforderlich oder zweckdienlich erscheint. Die Einladung erfolgt durch die Präsidentin oder den Präsidenten.

### §6 Anwesenheit

1. Die Mitglieder und Stellvertreter\_innen des Studierendenparlaments haben sich in die vom Präsidium auszulegende Anwesenheitsliste einzutragen. Beim frühzeitigen Verlassen der Sitzung muss sich das Mitglied oder die/der Stellvertreter\_in aus der ausliegenden Anwesenheitsliste austragen.
2. Gäste haben sich in eine vom Präsidium auszulegende Gästeliste einzutragen. Diese können sich mit einem Pseudonym eintragen, sofern sie die Information über ihren Klarnamen zuvor beim Präsidium hinterlegt haben. Dies ist mit datenschutzrechtlicher Begründung möglich.
3. Sind Mitglieder des AStAs an der Teilnahme an einer Sitzung des Studierendenparlamentes verhindert, haben sie sich vor der Sitzung bei dem Präsidium zu entschuldigen (§18 VI OrgS). Das Präsidium teilt dies unter dem Tagesordnungspunkt „Mitteilungen des Präsidiums“ mit.
4. Anwesend ist, wer sich namentlich in die Anwesenheitsliste eingetragen hat und sich im Parlamentsraum befindet.

## Abschnitt II: Tagesordnung

### §7 Aufstellung

1. Das Präsidium soll den Termin der Aufstellung der vorläufigen Tagesordnung 7 Tage im Voraus ankündigen.
2. Die Vorläufige Tagesordnung wird von dem Präsidium aufgestellt. Hierzu sind Anträge auf Beschlussfassung in Textform bis spätestens zum Tag der Aufstellung der Tagesordnung im AStA-Sekretariat oder direkt beim Präsidium des Studierendenparlaments einzureichen. Anträge können darüber hinaus bis spätestens zur Beschlussfassung über die endgültige Tagesordnung gestellt werden.

### §8 Inhalt

1. Die Tagesordnung muss mindestens enthalten:
    a) Feststellung der ordnungsgemäßen Ladung und der Beschlussfähigkeit;
    b) Bestellung zweier Schriftführerinnen oder Schriftführer;
    c) Beschlussfassung über die endgültige Tagesordnung;
    d) Genehmigung des Protokolls der letzten Sitzung;
    e) Mitteilungen des Präsidiums;
    f) Bericht aus dem AStA,
    g) Fragen an den AStA;
    h) Aussprache zum Bericht aus dem AStA;
    i) Verschiedenes.
2. Soweit erforderlich, sind auf die Tagesordnung aufzunehmen:
    a) Bericht aus dem Haushaltsausschuss;
    b) Bekanntmachungen;
    c) Beratungsanordnungen;
    d) Anfragen.
3. Die Neuwahl der Mitglieder des Präsidiums folgt unmittelbar der Bestellung zweier Schriftführerinnen oder Schriftführer. Im Übrigen sollen die in Absatz 1 genannten Tagesordnungspunkte anderen Tagesordnungspunkte vorausgehen. Ausgenommen ist Absatz 1 Buchstabe i), der stets letzter Tagesordnungspunkt ist. Der Bericht aus dem Haushaltsausschuss hat dem Bericht aus dem AStA, den Fragen an den AStA und der Aussprache zum Bericht aus dem AStA unmittelbar zu folgen. Inhaltliche Anträge gehen Wahlen stets voraus.

### §9 Antrags- und Anfragerecht

Jedes Mitglied der Studierendenschaft hat beim Studierendenparlament ein Antrags- und Anfragerecht (§5 II OrgS).

### §10 Aufnahme und Entfernung von Anträgen

1. Das Präsidium hat Anträge, die ihrem Wesen nach nicht vor Aufstellung der Tagesordnung in Schriftform eingereicht werden konnten (Dringlichkeitsanträge), zusätzlich ohne Abstimmung auf die endgültige Tagesordnung aufzunehmen.
2. Das Studierendenparlament entscheidet abweichend von §24 Abs. 2 durch Beschluss, ob Anträge, die in Schriftform vorliegen und keine Dringlichkeitsanträge im Sinne von Absatz 1 sind, auf die Tagesordnung aufgenommen werden. Der Beschluss hat ohne vorausgegangene Begründung und Beratung des Antrages zu erfolgen.
3. Die Entfernung eines Antrages von der Tagesordnung kann nicht beschlossen werden, wenn mindestens 10 Mitglieder widersprechen.

### §11 Anfragen außerhalb der aufgestellten Tagesordnung

Zu Beginn jeder ordentlichen Sitzung können von Mitgliedern der Studierendenschaft kurze Anfragen wenigstens in Textform gestellt werden, deren Wortlaut dem Präsidium vor Beginn der Sitzung vorliegen muss. Die Präsidentin oder der Präsident verliest die vom Präsidium zugelassenen Anfragen. Diese Anfragen müssen vom Studierendenparlament behandelt oder für eine der folgenden Sitzungen auf die vorläufige Tagesordnung aufgenommen werden.

## Abschnitt III: Verhandlungsordnung

### §12 Fraktionspausen

Auf Antrag der Fraktionen genehmigt das Präsidium abweichend von §24 Abs. 2 Fraktionspausen in angemessener und zumutbarer Länge. Die Genehmigung kann mit Mehrheit rückgängig gemacht werden. Genehmigt das Präsidium eine beantragte Fraktionspause nicht, so kann sie das Studierendenparlament mit relativer Mehrheit genehmigen. Nach Ablauf der genehmigten Fraktionspause tritt das Studierendenparlament wieder zusammen. Die Sitzung ist innerhalb einer Zeitspanne identisch mit der zuvor genehmigten Zeit, spätestens nach Ablauf weiterer 15 Minuten wieder zu eröffnen. Geschieht dies nicht, so gilt das Studierendenparlament als beschlussunfähig.

### §13 Aufnahme der Antragsberatung

Das Präsidium stellt Haupt- und Nebenanträge durch ausdrückliche Erklärung zur Beratung. Danach wird über sie nach Maßgabe der Vorschriften dieses Abschnittes beraten.

### §14 Haushaltsberatung

1. Bei der Beratung des Haushaltsplanes oder bei der Beratung von Nachtragehaushalten können Anträge, die zu einer Mehrausgabe oder Mindereinnahme gegenüber dem Haushaltsplan oder den Nachtraghaushalten führen, nur dann gestellt werden, wenn die Anträge gleichzeitig Vorschläge zur Deckung der durch sie entstehenden Mehrausgaben oder Mindereinnahmen enthalten. Antrag und Ausgleichsvorschlag bilden für die Beratung einen einheitlichen und unteilbaren Antrag.
2. Der Entwurf eines Haushaltsplanes kann im Studierendenparlament nicht beraten werden, wenn nicht der Haushaltsausschuss Gelegenheit zur vorherigen Beratung und Abgabe einer Empfehlung hatte (vgl. §6 I FinO).

### §15 Finanziell belastende Vorlagen und Anträge

Werden Vorlagen und Anträge im Studierendenparlament eingebracht, die geeignet sind, die Finanzgebarung der Studierendenschaft in der Gegenwart oder Zukunft zu belasten, und führen diese Mehrausgaben nach Auskunft der Finanzreferentin oder des Finanzreferenten zu einer Überschreitung des Haushaltsplanes, oder reichen nach Auskunft der Finanzreferentin oder des Finanzreferenten die im Haushaltsplan vorgesehenen Mittel nicht zur Deckung aus, so kann der Antrag nur beraten werden, wenn er den Vorgaben der FinO entspricht.

### §16 1. Lesung

In der Eingangsberatung (1. Lesung) begründet die Antragstellerin oder der Antragssteller ihren oder seinen Antrag. Das Studierendenparlament kann beschließen, den Antrag an einen Ausschuss zu überweisen, nicht in die Einzelberatung einzutreten oder diese zu vertagen.

### §17 2. Lesung

1. In der Einzelberatung (2. Lesung) stellt die Präsidentin oder der Präsident den Antrag abschnittsweise zur Beratung. In dieser werden Änderungs- und Ergänzungsanträge spätestens gestellt und behandelt. Diese müssen in Textform beim Präsidium eingereicht werden.
2. Änderungsanträge können von der Antragstellerin oder vom Antragsteller übernommen werden. Dies ist nicht der Fall, wenn ein Änderungsantrag sich auf einen bereits durch Abstimmung geänderten Inhalt bezieht.

### §18 3. Lesung

1. In der Schlussberatung (3. Lesung) wird der abstimmungsreife Antrag vorgestellt. Wenn zu diesem als Ganzem keine Wortmeldungen mehr vorliegen, erhält die Antragstellerin oder der Antragsteller das Schlusswort.
2. In der Schlussberatung können Anträge auf abschnittsweise Beschlussfassung, Vertagung oder Überweisung an einen Ausschuss gestellt werden.

### §19 Antrag auf Nichtbefassung

1. Ein Antrag auf Nichtbefassung ist nur vor dem Eintritt in die 1. Lesung zulässig. Er kann nicht in Bezug auf Geschäftsordnungsanträge gestellt werden.
2. Findet die Beratung über einen Nichtbefassungsantrag statt, so steht vor der Nichtbefassungsantragsantragstellerin oder dem Nichtbefassungsantragsteller der ursprünglichen Antragstellerin oder dem ursprünglichen Antragsteller ein Schlusswort zu; über den Antrag ist abzustimmen.

## Abschnitt IV: Redeordnung

### §20 Worterteilung

Das Präsidium berücksichtigt bei der Wortverteilung Häufigkeit der Wortbeiträge eines Redners oder einer Rednerin sowie das Geschlechterverhältnis auf der Redeliste. Ansonsten erteilt die Präsidentin oder der Präsident das Wort in der Reihenfolge der Wortmeldungen.

### §21 Beschränkung der Redezeit

1. Ein Antrag auf Beschränkung der Redezeit kann jederzeit gestellt werden. Die Beschränkung gilt entsprechend der Antragstellung bis zur nächsten Abstimmung oder bis zum nächsten Tagesordnungspunkt, spätestens jedoch bis zum nächsten Tagesordnungspunkt. Das Schlusswort kann nicht beschränkt werden.
2. Ein Antrag auf Beschränkung der Redezeit kann auch während eines Wortbeitrags beim Präsidium gestellt werden. Das Präsidium gibt diesem Antrag statt, sofern es eine Beschränkung für angemessen hält. Bei Widerspruch wird über den Antrag abgestimmt.
3. Die Redezeit darf nicht auf weniger als eine Minute beschränkt werden.

### §22 Schließung der Redeliste

1. Im Verlaufe der Beratung über einen Gegenstand kann abweichend von §24 Abs. 2 durch Beschluss des Studierendenparlamentes die Redeliste geschlossen werden. Wird ein Schluss der Redeliste beschlossen, besteht noch einmal die Möglichkeit, sich auf diese aufnehmen zu lassen.
2. Die Regelung nach Absatz 1 gilt für Personalwahlen entsprechend; sie gilt nicht für den Tagesordnungspunkt „Fragen an den AStA“.

### §23 Antrag auf Schluss der Debatte

1. Wird ein Antrag auf Schluss der Debatte gestellt, so ist abweichend von §24 Abs. 2 nach Anhören einer Gegenrednerin oder eines Gegenredners sofort darüber abzustimmen; der Beschluss bedarf einer Zweidrittelmehrheit der Mitglieder. Eine begründete Gegenrede geht einer formalen Gegenrede vor. Wird der Antrag angenommen, so steht nur noch der Antragstellerin oder dem Antragsteller des zur Beratung stehenden Antrages ein Schlusswort zu.
2. Bei Personalwahlen und dem Tagesordnungspunkt „Fragen an den AStA“ ist ein Antrag auf Schluss der Debatte ausgeschlossen.

### §24 Anträge und Ausführungen zum Verfahren und zur Geschäftsordnung

1. Mitglieder und Stellvertreter\_innen des Studierendenparlaments, die zum Verfahren Ausführungen machen oder Anträge zur Geschäftsordnung stellen wollen, erhalten das Wort außerhalb der Redeliste. Dazu werden aktuelle Redebeiträge vom Präsidium frühestens nach 5 Minuten unterbrochen. Ein Mitglied oder ein\_e Stellvertreter\_in des Studierendenparlaments, das das Wort zur Geschäftsordnung erhalten hat, darf sich nur zur verfahrensgemäßen Behandlung des gerade anstehenden Tagesordnungspunktes oder zum gestellten Geschäftsordnungsantrag äußern.
2. Sofern nach dieser Geschäftsordnung nicht etwas Anderes bestimmt ist, ist ein Geschäftsordnungsantrag angenommen, wenn nicht widersprochen wird. Bei Widerspruch ist bei Anhörung einer Gegenrednerin oder eines Gegenredners abzustimmen, bevor die Beratung zur Sache im Hinblick auf den gerade anstehenden Tagesordnungspunkt fortgesetzt wird.
3. Anträge zur Geschäftsordnung sind:
    a) Antrag auf Wechsel der Schriftführerinnen und Schriftführer [§4 II GO].
    b) Antrag auf Feststellung der Beschlussfähigkeit [§4 IV GO].
    c) Antrag auf Aufnahme eines Tagesordnungspunktes [§10 I GO].
    d) Antrag auf Fraktionspause [§12 GO].
    e) Antrag auf Nichtbefassung eines Antrages [§19 GO].
    f) Antrag auf Beschränkung der Redezeit [§21 StuPa-GO].
    g) Antrag auf Schließung der Redeliste [§22 StuPa-GO].
    h) Antrag auf Schluss der Debatte [§23 StuPa-GO].
    i) Antrag auf Sitzungsunterbrechung [§28 II StuPa-GO].
    j) Antrag auf geheime Abstimmung [§35 StuPa-GO].
    k) Antrag auf namentliche Abstimmung [§35 StuPa-GO].
    l) Antrag auf Rücknahme einer Ordnungsmaßnahme des Präsidiums [§27 StuPa-GO].
    m) Antrag auf Rederecht für Gäste [§25 III StuPa-GO].
    n) Antrag auf Vertagung eines TOP [§28 III StuPa-GO].
    o) Antrag auf Befragung zwischen zwei Wahlgängen [§34 III StuPa-GO].

### §25 Persönliche Erklärung

1. Nach Abschluss der Behandlung eines Tagesordnungspunktes können Anwesende das Wort zu einer persönlichen Erklärung erhalten. Die Rednerinnen oder Redner dürfen nicht zur Sache sprechen, sondern nur Äußerungen, die in der Aussprache in Bezug auf ihre Person vorgekommen sind, zurückweisen oder eigene Ausführungen richtigstellen. Sie dürfen nicht länger als drei Minuten sprechen.
2. Liegt die Erklärung wenigstens in Textform vor, so ist sie im Wortlaut in das Protokoll aufzunehmen.

### §26 Rederecht von beratenden Mitgliedern und Gästen

1. Beratende Mitglieder nach §12 VI OrgS dürfen immer an der Beratung teilnehmen.
2. Alle Studierenden haben Rederecht während der Sitzungen des Studierendenparlaments unter den Tagesordnungspunkten Mitteilungen, Anfragen, Fragen an den AStA, Aussprache über den Bericht des AStA sowie Verschiedenes. Sie haben außerdem Rederecht bei der Genehmigung des Protokolls im Falle der persönlichen Betroffenheit.
3. Gäste, die von ihrem Rederecht Gebrauch machen wollen, haben sich zuvor in eine vom Präsidium auszulegende Anwesenheitsliste einzutragen. Das Präsidium kann Gästen, abweichend von Absatz 2, ein Rederecht einräumen. Bei Gegenrede muss darüber abgestimmt werden.

### §27 Ordnungsrecht der Präsidentin oder des Präsidenten

1. Die Präsidentin oder der Präsident kann zur Ordnung und zur Sache rufen und nach zweimaliger Verwarnung das Wort entziehen, solange über den fraglichen Tagesordnungspunkt verhandelt wird. Bei ungebührlichem Benehmen einer anwesenden Person ist sie oder er berechtigt, die Person des Raumes zu verweisen.
2. Auf Antrag kann das Studierendenparlament beschließen, eine solche Maßnahme rückgängig zu machen. Hierbei ist die oder der Betroffene weder antrags- noch stimmberechtigt.

### §28 Sitzungsende, -unterbrechung, -vertagung

1. Die Sitzung wird geschlossen, wenn alle Tagesordnungspunkte behandelt sind.
2. Das Studierendenparlament kann auf Antrag eine Sitzungsunterbrechung beschließen, insbesondere um die Meinungsbildung im Hinblick auf den zur Beratung oder Abstimmung anstehenden Gegenstand zu erleichtern.
3. Das Studierendenparlament kann auf Antrag die Behandlung eines Tagesordnungspunktes vertagen, wenn sich bei der Beratung neue Gesichtspunkte ergeben haben, die einer ausführlichen Diskussion bedürfen, oder wenn ein anderer wichtiger Grund für die Vertagung vorliegt. Eine Vertagung soll nicht erfolgen, wenn die Behandlung eines Tagesordnungspunktes bereits einmal vertagt wurde.

## Abschnitt V: Abstimmung

### §29 Zeitpunkt der Abstimmung

Nach Abschluss der Verhandlungen über alle Haupt- und Nebenanträge ist nach Maßgabe der Vorschriften dieses Abschnittes abzustimmen.

### §30 Beschlussfassung

Das Studierendenparlament fasst, soweit satzungsmäßig nicht anders bestimmt, seine Beschlüsse mit einfacher Stimmenmehrheit bei Anwesenheit der Mehrheit seiner Mitglieder.

### §31 Einfache Stimmenmehrheit

Einfache Stimmenmehrheit bedeutet, dass die Zahl der Ja-Stimmen die der Nein-Stimmen überwiegt.

### §32 Mit der Mehrheit seiner Mitglieder (absolute Mehrheit)

Mit der Mehrheit seiner Mitglieder bedeutet, dass die Zahl der Ja-Stimmen mehr als die Hälfte der Anzahl der Mitglieder des Studierendenparlamentes beträgt.

### §33 Zweidrittelmehrheit

Zweidrittelmehrheit der Mitglieder bedeutet, dass die Zahl der Ja-Stimmen mindestens 2/3 der Anzahl der Mitglieder des Studierendenparlamentes beträgt.

### §34 Personenwahlen

1. Auf Personenwahlen finden die Bestimmungen der OrgS und dieser Geschäftsordnung über Beschlüsse Anwendung.
2. Kandidatinnen und Kandidaten dürfen sich dem Studierendenparlament vorstellen. Danach dürfen Fragen an die Kandidatin oder den Kandidaten gestellt werden.
3. Auf Antrag können vor jedem neuen Wahlgang Fragen an die Kandidatinnen und Kandidaten gestellt werden. Neu aufgestellte Kandidatinnen oder Kandidaten werden immer befragt.
4. Erklärt eine gewählte Person ihren Rücktritt, so ist frühestens nach Ablauf von sieben Tagen eine Sitzung zur Neuwahl einzuberufen. Dies gilt nicht, wenn der Rücktritt gelegentlich einer Neuwahl erklärt wird, zu der ordnungsgemäß geladen wurde, oder wenn mit der Rücktrittserklärung einem gleichzeitig anstehenden konstruktiven Misstrauensvotum vorgegriffen werden soll.

### §35 Geheime und namentliche Abstimmung

1. Auf Antrag eines Mitglieds oder einer/eines Stellvertreter\_indes Studierendenparlaments erfolgt die Abstimmung geheim oder namentlich. Einer Abstimmung über einen solchen Antrag bedarf es nicht. Der Antrag auf geheime Abstimmung hat Vorrang.
2. Bei namentlicher Abstimmung sind die Namen und das Stimmverhalten aller abstimmenden Mitglieder in das Protokoll aufzunehmen.

### §36 Mehrheit von Anträgen

Von mehreren zu einem Beratungspunkt vorliegenden Anträgen sind die unterschiedlich weitgehenden Anträge nacheinander, beginnend mit dem weitergehenden abzustimmen. Im Übrigen sind Anträge alternativ abzustimmen.

### §37 Entlastungen

Das Studierendenparlament kann eine Entlastung nicht beschließen, wenn nicht vorher die Stellungnahme des Haushaltsausschusses, der Bericht der Revisoren sowie gegebenenfalls eine Stellungnahme des zu entlastenden AStA gehört wurde.

## Abschnitt VI: Protokoll

### §38 Anfertigung, Inhalt und Veröffentlichung des Protokolls

1. Über die Verhandlungen im Studierendenparlament wird von den Schriftführerinnen oder Schriftführern ein Protokoll angefertigt. Das Protokoll wird vom Präsidium gegengelesen und erforderlichenfalls angepasst. Das Protokoll wird möglichst binnen 14 Tagen, aber spätestens 7 Tage vor der nächsten Sitzung des Studierendenparlamentes hochschulöffentlich zur Verfügung gestellt.
2. Dieses Protokoll enthält:
    a) eine Zusammenfassung der Berichte oder eine Kopie des in Textform zur Sitzung vorliegenden Berichts, und der Beantwortung der gestellten Fragen;
    b) die Namen der Kandidatinnen oder der Kandidaten für Personalwahlen, die (Listen-)Namen der vorschlagenden Mitglieder oder Stellvertreter\_innen des Studierendenparlamentes und das Ergebnis der Wahl;
    c) den Wortlaut aller Haupt- und Nebenanträge, die (Listen-)Namen der Antragstellerinnen oder Antragsteller und das Ergebnis der Abstimmung;
    d) Erklärungen zum Protokoll, die dem Präsidium wenigstens in Textform eingereicht werden müssen;
    e) Die Anwesenheitslisten.
3. Das Protokoll wird nach seiner Genehmigung durch das Studierendenparlament auf der Internetpräsenz des Studierendenparlamentes hochschulöffentlich zur Verfügung gestellt.
4. Auf geschlechtergerechte Sprache ist im Protokoll zu achten.

## Abschnitt VII: Sonstige Bestimmungen

### §39 Ausschüsse

1. Über den Vorsitz der Ausschüsse und Kommissionen entscheiden diese selbst, sofern das Studierendenparlament nicht etwas anders festlegt. Bis zur Wahl einer Vorsitzenden oder eines Vorsitzenden übernimmt ein Mitglied des Präsidiums des Studierendenparlamentes diese Aufgabe.
2. Die Ausschüsse und Kommissionen verhandeln in nichtöffentlicher Sitzung. Für ihre Verhandlungsweise gilt diese Geschäftsordnung, sofern sich der Ausschuss oder die Kommission keine eigene Geschäftsordnung gibt. §6 VI 2 OrgS (Ausschluss von Umlaufverfahren) gilt nicht für Ausschüsse und Kommissionen des Studierendenparlamentes.
3. In den Haushaltsausschuss und in alle Ausschüsse und Kommissionen des Studierendenparlamentes, die sich mit Hochschul- und Studienfragen sowie mit Fachschaftsfragen befassen, kann jeweils ein von der FSRV beratendes Mitglied entsandt werden (§15 I OrgS).

### §40 Haushaltsausschuss

1. Die Mitglieder des Haushaltsausschusses werden während der konstituierenden Sitzung des Studierendenparlaments nach §§10 IV, 15 III OrgS ernannt.
2. Der oder die Vorsitzende des Haushaltsausschusses soll aus der Opposition stammen.

### §41 Änderungen der Geschäftsordnung

Die Geschäftsordnung kann nur mit Zweidrittelmehrheit der Mitglieder im Sinne von §31 StuPa-GO geändert werden.

### §42 Inkrafttreten

Diese Geschäftsordnung tritt mit ihrem Beschluss in Kraft und wird in den Amtlichen Mitteilungen I der Georg-August-Universität Göttingen veröffentlicht. Zugleich tritt die Geschäftsordnung des Studentenparlaments der Georg-August-Universität in der zuvor geltenden Fassung außer Kraft.

























































