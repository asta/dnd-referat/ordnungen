---
title: >-
  Richtlinie zur Vergabe von Funktionsaccounts an studentische Gremien und
  Gruppen
date: 2021-07-05 17:47:10
tags: "Digitalisierung"
---

### § 1 Berechtigte Gruppen
1. Gremien der studentischen Selbstverwaltung erhalten einen GWDG-Funktionsaccount.
2. Bei der Uni eingetragene Hochschulgruppen erhalten auf Anfrage einen GWDG-Funktionsaccount.
3. Gruppen, die strukturell Vergleichbar zu Hochschulgruppen sind, erhalten auf Anfrage einen GWDG-Funktionsaccount.

### § 2 strukturell ähnliche Gruppen
Eine Gruppe gilt als vergleichbar zu einer Hochschulgruppe, wenn folgende Bedingungen erfüllt sind:
1. Die Gruppe weist ein vorwiegen studentisches Betätigungsfeld auf.
2. Die Gruppe plant über ein Semester hinaus aktiv zu sein.
3. Mindestens ein Mitglied der Gruppe ist an der Georg-August-Universität Göttingen immatrikuliert.

### § 3 Beantragung von Funktionsaccounts

1. Gruppen gemäß §1 Abs. 2 und 3 können AStA einen GWDG-Funktionsaccount beantragen.
2. Der Antrag umfasst mindestens folgende Angaben:
      a. Name der Gruppe
      b. Eine feste Ansprechperson
      c. Ggf. Nachweis über die Voraussetzungen nach §2
3. Die Gruppe informiert schnellstmöglich über eventuelle Änderungen an den Antragsdaten.

### § 4 Entscheidungsfindung und Ausnahmen

1. Im Regelfall entscheidet der für den AStA zuständige Administrator in Einvernehmen mit dem Referat Vorsitz. In begründeten Ausnahmefällen kann davon abgewichen werden. In diesem Fall ist ein Beschluss durch die AStA-Sitzung zu fassen.

### § 5 Löschung

1. GWDG-Funktionsaccounts von Gruppen gemäß §1 Abs. 2 und 3 können durch den AStA gelöscht werden, wenn sich seit über 2 Jahren nicht mehr in den Account eingeloggt wurde. Der AStA probiert dabei zunächst die Gruppe über die angegebene Kontaktperson zu kontaktieren.

### Übergangsbestimmungen
Bestehende Gruppen gemäß §1 Abs. 2 und 3 müssen ggf. eine Kontaktperson angeben.
