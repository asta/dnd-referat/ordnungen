---
title: Beitragsordnung der Studierendenschaft der Georg-August-Universität Göttingen (BeitrO)
date: 2021-09-11
tags:
  - Beitragsordnung
  - BeitrO
  - Studierendenschaft
fassung_der_bekanntmachung:
  date: 2006-04-13
  ort: Amtliche Mitteilungen 4/2006 S. 197
zuletzt_geaendert:
  gremium: Studierendenparlament
  date: 2021-07-01
  ort: Amtliche Mitteilungen I 40/2021 S. 956
---

Beitragsordnung der Studierendenschaft in der Fassung der Bekanntmachung vom 13.04.2006 (Amtliche Mitteilungen 4/2006 S. 197), zuletzt geändert durch Beschluss des Studierendenparlaments vom 01.07.2021 (Amtliche Mitteilungen I 40/2021 S. 956)

# Beitragsordnung der Studierendenschaft der Georg-August-Universität Göttingen (BeitrO)

## §1 Beitragshöhe

1. Die Höhe der Beiträge, die die Studierendenschaft zur Durchführung ihrer Aufgaben von ihren Mitgliedern erhebt, wird auf 14,- Euro festgelegt. [^1]
2. Der Sportanteil nach §50 Abs. 3 Lit. a) OrgS beträgt 2,- Euro. [^1]
3. Für das Bahnsemesterticket erhebt die Studierendenschaft im Wintersemester 2008/2009 einen zusätzlichen Betrag von 63,29 Euro und im Sommersemester 2009 einen zusätzlichen Beitrag von 62,29 Euro. Für das Bahnsemesterticket erhebt die Studierendenschaft im Wintersemester 2009/2010 und im Sommersemester 2010 einen zusätzlichen Beitrag von 65,49 Euro. Für das Bahnsemesterticket (ME, CAN) erhebt die Studierendenschaft im Wintersemester 2010/2011 und im Sommersemester 2011 einen zusätzlichen Beitrag von 25,57 Euro. Für das Bahnsemesterticket (DB, NWB, ERB) erhebt die Studierendenschaft im Wintersemester 2010/2011 und im Sommersemester 2011 einen zusätzlichen Beitrag von 42,24 Euro. Für das Bahnsemesterticket erhebt die Studierendenschaft im Wintersemester 2011/2012 einen zusätzlichen Betrag von 79,57 Euro und im Sommersemester 2012 einen zusätzlichen Beitrag von 77,04 Euro. Für das Bahnsemesterticket erhebt die Studierendenschaft im Wintersemester 2012/2013 einen zusätzlichen Beitrag von 87,62 Euro und im Sommersemester 2013 einen zusätzlichen Beitrag von 87,86 Euro. Für das Kunst- und Kultursemesterticket erhebt die Studierendenschaft im Wintersemester 2012/2013 und im Sommersemester 2013 einen zusätzlichen Betrag von 7,60 Euro. Für das Bahnsemesterticket erhebt die Studierendenschaft im Wintersemester 2013/2014 einen zusätzlichen Beitrag von 95,04 Euro und im Sommersemester 2014 einen zusätzlichen Beitrag von 96,19 Euro. Für das Kunst- und Kultursemesterticket erhebt die Studierendenschaft im Wintersemester 2013/2014 und im Sommersemester 2014 einen zusätzlichen Betrag von 9,30 Euro. Für das Bahnsemesterticket erhebt die Studierendenschaft im Wintersemester 2014/2015 einen zusätzlichen Beitrag von 105,43 Euro und im Sommersemester 2015 einen zusätzlichen Beitrag von 107,24 Euro. Für das Kunst- und Kultursemesterticket erhebt die Studierendenschaft im Wintersemester 2014/2015 und im Sommersemester 2015 einen zusätzlichen Betrag von 9,80 Euro. Für das Bussemesterticket erhebt die Studierendenschaft im Wintersemester 2014/2015 einen zusätzlichen Beitrag von 26,90 Euro und im Sommersemester 2015 einen zusätzlichen Beitrag von 26,90 Euro. Für das Bahnsemesterticket erhebt die Studierendenschaft im Wintersemester 2015/16 einen zusätzlichen Beitrag von 107,51 € und im Sommersemester 2016 einen zusätzlichen Beitrag von 108,67 €. Für das Bussemesterticket erhebt die Studierendenschaft im Wintersemester 2015/16 einen zusätzlichen Beitrag von 28,90 € und im Sommersemester 2016 einen zusätzlichen Beitrag von 28,90 €. Für das Kultursemesterticket erhebt die Studierendenschaft im Wintersemester 2015/16 einen zusätzlichen Beitrag von 9,00 € und im Sommersemester einen zusätzlichen Beitrag von 8,40 €. Für das Bahnsemesterticket erhebt die Studierendenschaft im Wintersemester 2016/17 einen zusätzlichen Beitrag von 115,78 € und im Sommersemester 2017 einen zusätzlichen Beitrag von 115,85 €. Für das Bussemesterticket erhebt die Studierendenschaft im Wintersemester 2016/17 einen zusätzlichen Beitrag von 34,50 € und im Sommersemester 2017 einen zusätzlichen Beitrag von 34,50 €. Für das Kultursemesterticket erhebt die Studierendenschaft im Wintersemester 2016/17 einen zusätzlichen Beitrag von 9,50 € und im Sommersemester 2017 einen zusätzlichen Beitrag von 9,50 €. Für das Bahnsemesterticket erhebt die Studierendenschaft im Wintersemester 2017/18 einen zusätzlichen Beitrag von 126,30 € und im Sommersemester 2018 einen zusätzlichen Beitrag von 126,36 €. Für das Bussemesterticket erhebt die Studierendenschaft im Wintersemester 2017/18 und im Sommersemester 2018 je einen zusätzlichen Beitrag von 39,90 €. Für das Kulturticket erhebt die Studierendenschaft im Wintersemester 2017/2018 und Sommersemester 2018 jeweils einen zusätzlichen Betrag von 9,75 €. Für das landesweite Bahnsemesterticket erhebt die Studierendenschaft einen zusätzlichen Beitrag: im Wintersemester 2018/19 und im Sommersemester 2019 jeweils 133,19 Euro, im Wintersemester 2019/20 und im Sommersemester 2020 jeweils 135,42 Euro je Studierender oder je Studierendem. Für das Bussemesterticket erhebt die Studierendenschaft im Wintersemester 2018/19 und im Sommersemester 2019 je einen zusätzlichen Beitrag von 44,40 Euro je Studierender oder je Studierendem. Für das Kunst- und Kulturticket erhebt die Studierendenschaft im Wintersemester 2018/2019 und Sommersemester 2019 je einen zusätzlichen Betrag von 9,81 Euro je Studierender oder je Studierendem. Für das Bussemesterticket erhebt die Studierendenschaft im Wintersemester 2019/20 und im Sommersemester 2020 je einen zusätzlichen Beitrag von 48,90 Euro je Studierender oder je Studierendem. Für das Kultursemesterticket erhebt die Studierendenschaft für das Wintersemester 2019/20 und Sommersemester 2020 je einen zusätzlichen Beitrag in Höhe von 9,99 Euro je Studierender oder je Studierendem. Für das landesweite Bahnsemesterticket erhebt die Studierendenschaft einen zusätzlichen Beitrag: im Wintersemester 2020/2021 und im Sommersemester 2021 jeweils 139,20 Euro sowie für das Leistungsangebot des Nordhessischen Verkehrsverbundes jeweils 3,72 Euro je Studierender oder je Studierendem. Für das Bussemesterticket STADT erhebt die Studierendenschaft im Wintersemester 2020/2021 und Sommersemester 2021 je einen zusätzlichen Beitrag von 50,90 € je Studierender oder je Studierendem. Für das Kultursemesterticket erhebt die Studierendenschaft für das Wintersemester 2020/21 und Sommersemester 2021 je einen zusätzlichen Beitrag in Höhe von 9,84 Euro je Studierender oder je Studierendem. Für das landesweite Bahnsemesterticket erhebt die Studierendenschaft einen zusätzlichen Beitrag: im Wintersemester 2021/22 und im Sommersemester 2022 jeweils 138,27 Euro zzgl. 3,80 Euro für die zusätzliche Vereinbarung mit dem Nordhessischen Verkehrsverbund, je Studierender oder je Studierendem. Für das Bussemesterticket STADT erhebt die Studierendenschaft im Wintersemester 2021/2022 und Sommersemester 2022 je einen zusätzlichen Beitrag von 50,90 € je Studierender oder je Studierendem. Für das Kultursemesterticket erhebt die Studierendenschaft im Wintersemester 2021/2022 und Sommersemester 2022 je einen zusätzlichen Beitrag von 11,14 € je Studierender oder je Studierendem.


[^1]: Die geänderten Beiträge werden erstmals für das Wintersemester 2021/22 erhoben.


## §2 Beitragspflicht

1. Beitragspflichtig sind alle Mitglieder der Studierendenschaft der Georg-August-Universität Göttingen.
2. Beurlaubte Studierende werden für die Zeit ihrer Beurlaubung von der Beitragszahlung befreit.
3. Studierende, die im Rahmen eines Doppelpromotionsabkommens an einer weiteren Hochschule immatrikuliert sind, werden auf Antrag von der Zahlung der Beiträge für das Semester befreit, in dem sie sich auf Grund des Doppelpromotionsabkommens überwiegend an der anderen Hochschule aufhalten, sofern sie Beiträge an die dortige Studierendenschaft entrichten. Studierende, die auf der Grundlage einer Kooperationsvereinbarung an einer weiteren Hochschule immatrikuliert sind, werden auf Antrag von der Zahlung der Beiträge in einem dem Verhältnis entsprechenden Umfang, in dem die Universität Göttingen nach der Kooperationsvereinbarung auf die Erhebung der für sie erhobenen Studienbeiträge beziehungsweise Studiengebühren verzichtet, frei gestellt.
4. Studierende der Georg-August-Universität Göttingen, die ausschließlich in einem berufsbegleitenden Studiengang immatrikuliert sind, sind von der Zahlung des Beitrags nach §1 Absatz 3 ab dem Wintersemester 2020/2021 befreit.

## §3 Fälligkeit

1. Die Beiträge sind bei der Immatrikulation oder Rückmeldung fällig und werden von der Hochschule eingezogen.
2. Die Beiträge können nicht gestundet und nicht erlassen werden, sofern diese Ordnung nichts anderes bestimmt. Erfolgt
    a) die Exmatrikulation oder
    b) ein Antrag auf Rücknahme der Immatrikulation oder auf Exmatrikulation

    vor oder innerhalb eines Monats nach Vorlesungsbeginn, werden die geleisteten Beiträge auf Antrag erstattet, sofern der Studienausweis innerhalb der genannten Frist beim Studierendenbüro eingegangen ist.
3. Die Beiträge unterliegen dem Verwaltungszwangsverfahren. Der Anspruch auf die Beiträge verjährt in drei Jahren.

## §4 Rückerstattung von Beiträgen

1. Bereits entrichtete Beiträge für die Semestertickets Bus und Bahn werden vom AStA auf Antrag an die Beitragspflichtigen zurückerstattet, soweit diese nach dem Schwerbehindertengesetz einen Anspruch auf unentgeltliche Beförderung im öffentlichen Personennahverkehr haben. Der Antrag muss enthalten:
    a) das vollständig ausgefüllte und unterschriebene Formblatt gemäß Anlage 1[^2],
    b) Behindertenausweis in Kopie,
    c) eine Immatrikulationsbescheinigung des Antragssemesters.
2. Eine anteilige Rückerstattung der bereits entrichteten Beiträge für die Semestertickets Bus und Bahn ist auf Antrag möglich für Studierende, welche an einer weiteren niedersächsischen Hochschule immatrikuliert sind und dort ebenfalls ein Bahnsemesterticket erstehen müssen, soweit die Verträge dies umfassen. Der Antrag muss enthalten:
    a) das vollständig ausgefüllte und unterschriebene Formblatt gemäß Anlage 2[^3],
    b) Immatrikulationsbescheinigung und Semesterticket der zweiten niedersächsischen Hochschule in Kopie,
    c) eine Immatrikulationsbescheinigung des Antragssemesters.
3. Die Anträge nach Abs. 1 bis 2 haben die Ausschlussfristen: SoSe 30. Mai, 23:59 und WiSe: 30. November, 23:59. Der AStA gibt die Frist für jedes Semester öffentlich bekannt. Unvollständige Anträge können nicht bearbeitet werden. Können für den Antrag erforderliche Unterlagen nicht innerhalb der Antragsfrist erbracht werden und hat die oder der Antragsstellende dies nicht zu vertreten, so kann, wenn die Gründe für dieses Versäumnis gegenüber dem AStA nachgewiesen werden, eine Fristverlängerung gewährt werden.

[^2]: [Anlage 1 - Antrag auf Rückerstattung des Bus- und Bahnsemestertickets für schwerbehinderte Personen](Beitragsordnung-BeitrO-Anlage-1.pdf)
[^3]: [Anlage 2 - Bus- und Bahn-Semesterticketrückerstattung](Beitragsordnung-BeitrO-Anlage-2.pdf)

## §5 Inkrafttreten

Diese Beitragsordnung tritt am Tage nach ihrer Veröffentlichung in den Amtlichen Mitteilungen der Georg-August-Universität Göttingen in Kraft.



















