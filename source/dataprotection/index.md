---
title: Datenschutzerklärung
date: 2021-07-06 01:41:42
---

Diese Datenschutzerklärung klärt Sie über die Art, den Umfang und Zweck der Verarbeitung von personenbezogenen Daten (nachfolgend kurz „Daten“) im Rahmen der Erbringung unserer Leistungen sowie innerhalb unseres Onlineangebotes und der mit ihm verbundenen Webseiten, Funktionen und Inhalte sowie externen Onlinepräsenzen, wie z.B. unser Social Media Profile auf (nachfolgend gemeinsam bezeichnet als „Onlineangebot“). Im Hinblick auf die verwendeten Begrifflichkeiten, wie z.B. „Verarbeitung“ oder „Verantwortlicher“ verweisen wir auf die Definitionen im Art. 4 der Datenschutzgrundverordnung (DSGVO).

Verantwortlicher
Allgemeiner Studierendenausschuss (AStA)
Goßlerstraße 16a
37073 Göttingen

Tel.: +49 (0)551 / 39-4564
V.i.S.d.P: Pippa Schneider (AStA-Vorsitzende)

Kontakt: sekretariat@asta.uni-goettingen.de

Datenschutzbeauftragter der Studierendenschaft: Sergio Perez
Kontakt: datenschutz@asta.uni-goettingen.de

Datenschutzbeauftragter der Universität Göttingen: Prof. Dr. Andreas Wiebe
stellv. Datenschutzbeauftragter der Universität Göttinfgen: Florian Hallaschka
Mailadresse: datenschutz@uni-goettingen.de
Link: https://www.uni-goettingen.de/de/576209.html

Bei der Nutzung dieser Website fallen ausschließlich Meta-/Kommunikationsdaten an, die für den Betrieb von Websiten technisch notwendig sind (z.B. IP-Adressen).
Eine möglichst datensparsame (d.h. möglichst anonyme) Nutzung wird angestrebt.
