const fs = require("fs");
const execa = require("execa");

hexo.extend.filter.register('before_post_render', function(data){
    if(data.layout !== "post")
        return data;
    return (async () => {
        await execa(`mkdir`, [`-p`, `${data.asset_dir}`]);
        const result = await execa(`pandoc`, [
            data.full_source,
            `-o`,
            `${data.asset_dir}${data.slug}.pdf`
        ]);
        if(result.exitCode !== 0)
            console.error(`Error generating pdf file for ${data.source}.`)
        data.pdfFile = `${data.slug}.pdf`;
        return data;
    })();
});
